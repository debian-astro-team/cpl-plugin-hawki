
# Update or create ESO PRO CATG keyword in PHU of input file with user-supplied string

from __future__ import absolute_import
from __future__ import print_function
try:
    from optparse import OptionParser
    import numpy as np
    import pyfits as pf
    import copy
    import sys
except:
    print("Error loading one or more modules")
    sys.exit()

parser = OptionParser()
options, args = parser.parse_args()

if len(args) < 2:
    print("Not enough arguments")
    print("Usage: update_procatg.py FILE VALUE")
    print("Updates (or creates) PRO.CATG PHU keyword with value=VALUE, e.g. REFERENCE_DARK")
    sys.exit()

try:
    file=pf.open(args[0],mode='update')
except:
    print("Error: " + args[0] + " not readable")
    sys.exit()

file[0].header['HIERARCH ESO PRO CATG'] = args[1]

try:
    file.flush(output_verify='fix',verbose=True)
    file.close()
except:
    print("Error: Could not write to file " + args[0])
