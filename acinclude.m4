
# HAWKI_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([HAWKI_SET_VERSION_INFO],
[
    hawki_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    hawki_major_version=`echo "$hawki_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    hawki_minor_version=`echo "$hawki_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    hawki_micro_version=`echo "$hawki_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$hawki_major_version"; then hawki_major_version=0
    fi

    if test -z "$hawki_minor_version"; then hawki_minor_version=0
    fi

    if test -z "$hawki_micro_version"; then hawki_micro_version=0
    fi

    HAWKI_VERSION="$hawki_version"
    HAWKI_MAJOR_VERSION=$hawki_major_version
    HAWKI_MINOR_VERSION=$hawki_minor_version
    HAWKI_MICRO_VERSION=$hawki_micro_version

    if test -z "$4"; then HAWKI_INTERFACE_AGE=0
    else HAWKI_INTERFACE_AGE="$4"
    fi

    HAWKI_BINARY_AGE=`expr 100 '*' $HAWKI_MINOR_VERSION + $HAWKI_MICRO_VERSION`
    HAWKI_BINARY_VERSION=`expr 10000 '*' $HAWKI_MAJOR_VERSION + \
                          $HAWKI_BINARY_AGE`

    AC_SUBST(HAWKI_VERSION)
    AC_SUBST(HAWKI_MAJOR_VERSION)
    AC_SUBST(HAWKI_MINOR_VERSION)
    AC_SUBST(HAWKI_MICRO_VERSION)
    AC_SUBST(HAWKI_INTERFACE_AGE)
    AC_SUBST(HAWKI_BINARY_VERSION)
    AC_SUBST(HAWKI_BINARY_AGE)

    AC_DEFINE_UNQUOTED(HAWKI_MAJOR_VERSION, $HAWKI_MAJOR_VERSION,
                       [HAWKI major version number])
    AC_DEFINE_UNQUOTED(HAWKI_MINOR_VERSION, $HAWKI_MINOR_VERSION,
                       [HAWKI minor version number])
    AC_DEFINE_UNQUOTED(HAWKI_MICRO_VERSION, $HAWKI_MICRO_VERSION,
                       [HAWKI micro version number])
    AC_DEFINE_UNQUOTED(HAWKI_INTERFACE_AGE, $HAWKI_INTERFACE_AGE,
                       [HAWKI interface age])
    AC_DEFINE_UNQUOTED(HAWKI_BINARY_VERSION, $HAWKI_BINARY_VERSION,
                       [HAWKI binary version number])
    AC_DEFINE_UNQUOTED(HAWKI_BINARY_AGE, $HAWKI_BINARY_AGE,
                       [HAWKI binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# HAWKI_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([HAWKI_SET_PATHS],
[
    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}/'
    fi
    if test -z "$apidocdir"; then
        apidocdir='${pipedocsdir}/html'
    fi


    if test -z "$configdir"; then
        configdir='${datadir}/${PACKAGE}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    if test -z "$scriptsdir"; then
       scriptsdir='${datadir}/esopipes/${PACKAGE}-${VERSION}/scripts'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(apidocdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)
    AC_SUBST(scriptsdir)

    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(HAWKI_PLUGIN_DIR,"esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(HAWKI_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# HAWKI_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([HAWKI_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    HAWKI_INCLUDES='-I$(top_srcdir)/hawki -I$(top_srcdir)/irplib -I$(top_srcdir)/casu/src -I$(top_srcdir)/casu/src/catalogue'
    HAWKI_LDFLAGS='-L$(top_builddir)/hawki -L$(top_srcdir)/casu/src/catalogue'

    all_includes='$(HAWKI_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(HAWKI_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    # Library aliases

    LIBHAWKI='$(top_builddir)/hawki/libhawki.la'
    LIBCASU='$(top_builddir)/casu/src/libcasu.la'
    LIBCASUCAT='$(top_builddir)/casu/src/catalogue/libcasu_catalogue.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'

    # Substitute the defined symbols

    AC_SUBST(HAWKI_INCLUDES)
    AC_SUBST(HAWKI_LDFLAGS)

    AC_SUBST(LIBHAWKI)
    AC_SUBST(LIBIRPLIB)
    AC_SUBST(LIBCASU)
    AC_SUBST(LIBCASUCAT)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(HAWKI_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(HAWKI_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])

# HAWKI_CHECK_OPENMP_LIBS
# -----------------------
# Add the OpenMP libraries to linking libraries
AC_DEFUN([HAWKI_CHECK_OPENMP_LIBS],
[
    if test -n "$OPENMP_CFLAGS" ; then
        AC_SEARCH_LIBS([omp_get_num_threads], [gomp mtsk omp], [],
              AC_MSG_ERROR([OpenMP runtime environment not found!]))
    fi

])
