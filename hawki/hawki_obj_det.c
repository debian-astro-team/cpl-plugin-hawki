/* $Id: hawki_obj_det.c,v 1.3 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <float.h>
#include <string.h>
#include <math.h>
#include <cpl.h>

#include "hawki_dfs_legacy.h"
#include "hawki_utils_legacy.h"
#include "hawki_pfits_legacy.h"
#include "hawki_obj_det.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_obj_det  Functionality related to object detection
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Compute statistics on the object properties
  @param    objs_prop       the table with the properties of the objects (IN)
  @param    objs_prop_stats the property lists containing statistics on
                            the previous table (OUT)
  @return   0 if ok, -1 in error case
 
  The statistics computed are the mean, median, minimum, maximum and stdev.
  Currently, the statistics ar ecomputed upon the angle of the object and 
  the ellipticity.

 */
int hawki_obj_prop_stats
(cpl_table ** objs_prop, cpl_propertylist ** objs_prop_stats)
{
    cpl_errorstate  prestate = cpl_errorstate_get();
    int             idet;
    
    /* Checking input */
    if(objs_prop == NULL || objs_prop_stats == NULL)
    {
        cpl_msg_error(__func__, "The property list or the tel table is null");
        return -1;
    }
    
    /* Compute the statistics */
    for(idet = 0; idet < HAWKI_NB_DETECTORS; ++idet)
    {
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ANGLE MEAN",
             cpl_table_get_column_mean(objs_prop[idet], HAWKI_COL_OBJ_ANGLE));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ANGLE MED",
             cpl_table_get_column_median(objs_prop[idet], HAWKI_COL_OBJ_ANGLE));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ANGLE MIN",
             cpl_table_get_column_min(objs_prop[idet], HAWKI_COL_OBJ_ANGLE));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ANGLE MAX",
             cpl_table_get_column_max(objs_prop[idet], HAWKI_COL_OBJ_ANGLE));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ANGLE STDEV",
             cpl_table_get_column_stdev(objs_prop[idet], HAWKI_COL_OBJ_ANGLE));

        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ELLIP MEAN",
             cpl_table_get_column_mean(objs_prop[idet], HAWKI_COL_OBJ_ELLIP));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ELLIP MED",
             cpl_table_get_column_median(objs_prop[idet], HAWKI_COL_OBJ_ELLIP));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ELLIP MIN",
             cpl_table_get_column_min(objs_prop[idet], HAWKI_COL_OBJ_ELLIP));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ELLIP MAX",
             cpl_table_get_column_max(objs_prop[idet], HAWKI_COL_OBJ_ELLIP));
        cpl_propertylist_append_double
            (objs_prop_stats[idet], "ESO QC OBJ ELLIP STDEV",
             cpl_table_get_column_stdev(objs_prop[idet], HAWKI_COL_OBJ_ELLIP));
    }
    
    if(!cpl_errorstate_is_equal(prestate))
        return -1;
    return 0;
}

/**@}*/

