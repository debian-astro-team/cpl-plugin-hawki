/* $Id: hawki_var.h,v 1.4 2015/08/07 13:07:06 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:06 $
 * $Revision: 1.4 $
 * $Name:  $
 */


/* Includes */

#ifndef HAWKI_VAR_H
#define HAWKI_VAR_H

#include <casu_fits.h>
#include <casu_mask.h>

/* Function prototypes */

extern casu_fits *hawki_var_create(casu_fits *in, casu_mask *mask, 
                                   float readnoise, float gain);
extern void hawki_var_add(casu_fits *in1, casu_fits *in2);
extern void hawki_var_div_im(casu_fits *in1, casu_fits *in2);
extern void hawki_var_divk(casu_fits *in, float kscl);

#endif

/* 

$Log: hawki_var.h,v $
Revision 1.4  2015/08/07 13:07:06  jim
Fixed copyright to ESO

Revision 1.3  2015/01/29 12:00:16  jim
Modified comments + removed redundant routines

Revision 1.2  2014/12/12 21:45:28  jim
added a few more routines

Revision 1.1  2014/12/11 12:26:21  jim
new entry


*/

