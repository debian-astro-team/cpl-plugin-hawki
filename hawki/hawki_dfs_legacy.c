/* 
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <string.h>
#include <math.h>

#include <cpl.h>

#include "hawki_dfs_legacy.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_dfs  DFS related functions
 *
 * TBD
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Set the group as RAW or CALIB in a frameset
  @param    set     the input frameset
  @return   0 if ok, -1 in error case
 */
/*----------------------------------------------------------------------------*/
int hawki_dfs_set_groups_legacy(cpl_frameset * set)
{
    cpl_frame   *   cur_frame ;
    const char  *   tag ;
    int             nframes ;
    int             i ;

    /* Check entries */
    if (set == NULL) return -1 ;

    /* Initialize */
    nframes = cpl_frameset_get_size(set) ;

    /* Loop on frames */
    for (i=0 ; i<nframes ; i++) {
        cur_frame = cpl_frameset_get_position(set, i) ;
        tag = cpl_frame_get_tag(cur_frame) ;

        /* RAW frames */
        if (!strcmp(tag, HAWKI_COMMAND_LINE)                       ||
                !strcmp(tag, HAWKI_CAL_DARK_RAW)                   ||
                !strcmp(tag, HAWKI_TEC_FLAT_RAW)                   ||
                !strcmp(tag, HAWKI_CAL_FLAT_RAW)                   ||
                !strcmp(tag, HAWKI_CAL_ZPOINT_RAW)                 ||
                !strcmp(tag, HAWKI_CAL_ILLUM_RAW)                  ||
                !strcmp(tag, HAWKI_CAL_DISTOR_RAW)                 ||
                !strcmp(tag, HAWKI_IMG_JITTER_SKY_RAW)             ||
                !strcmp(tag, HAWKI_IMG_JITTER_RAW)                 ||
                !strcmp(tag, HAWKI_CAL_LINGAIN_LAMP_RAW)           ||
                !strcmp(tag, HAWKI_CAL_LINGAIN_DARK_RAW)           ||
                !strcmp(tag, HAWKI_CALPRO_BASICCALIBRATED)         ||
                !strcmp(tag, HAWKI_CALPRO_SKY_BASICCALIBRATED)     ||
                !strcmp(tag, HAWKI_CALPRO_BKGIMAGE)                ||
                !strcmp(tag, HAWKI_CALPRO_BKG_SUBTRACTED)          ||
                !strcmp(tag, HAWKI_CALPRO_DIST_CORRECTED)          ||
                !strcmp(tag, HAWKI_CALPRO_COMBINED)                ||
                !strcmp(tag, HAWKI_CALPRO_OBJ_MASK)                ||
                !strcmp(tag, HAWKI_CALPRO_ZPOINT_TAB))
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_RAW) ;
        /* CALIB frames */
        else if (!strcmp(tag, HAWKI_CALPRO_BPM)          ||
                !strcmp(tag, HAWKI_UTIL_STDSTARS_RAW)    ||
                !strcmp(tag, HAWKI_UTIL_DISTMAP_RAW)     ||
                !strcmp(tag, HAWKI_CALPRO_BPM_HOT)       ||
                !strcmp(tag, HAWKI_CALPRO_BPM_COLD)      ||
                !strcmp(tag, HAWKI_CALPRO_FLAT)          ||
                !strcmp(tag, HAWKI_CALPRO_DARK)          ||
                !strcmp(tag, HAWKI_CALPRO_STDSTARS)      ||
                !strcmp(tag, HAWKI_CALPRO_DISTORTION_X)  ||
                !strcmp(tag, HAWKI_CALPRO_DISTORTION_Y)  ||
                !strcmp(tag, HAWKI_CALPRO_DISTORTION))
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_CALIB) ;
    }
    return 0 ;
}

/**@}*/
