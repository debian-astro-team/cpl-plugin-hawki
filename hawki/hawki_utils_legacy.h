/* $Id: hawki_utils.h,v 1.44 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.44 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_UTILS_H
#define HAWKI_UTILS_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                New types
 -----------------------------------------------------------------------------*/

typedef enum _HAWKI_BAND_ {
    HAWKI_BAND_J,
    HAWKI_BAND_H,
    HAWKI_BAND_K,
    HAWKI_BAND_Y,
    HAWKI_BAND_UNKNOWN
} hawki_band ;

/*-----------------------------------------------------------------------------
   								Define
 -----------------------------------------------------------------------------*/

#define HAWKI_NB_DETECTORS          4
#define HAWKI_PHOT_STAR_RADIUS      30.0
#define HAWKI_DET_NPIX_X            2048
#define HAWKI_DET_NPIX_Y            2048

/* Input from commissionning 1 report */
#define HAWKI_DET1_POSX             0
#define HAWKI_DET1_POSY             0
#define HAWKI_DET2_POSX             2048 + 153
#define HAWKI_DET2_POSY             0 + 3
#define HAWKI_DET3_POSX             2048 + 157
#define HAWKI_DET3_POSY             2048 + 144
#define HAWKI_DET4_POSX             0 + 5
#define HAWKI_DET4_POSY             2048 + 142

#define HAWKI_NB_VC                 32

#define HAWKI_HEADER_EXT_FORWARD    "DET CHIP|DET WIN NX|DET WIN NY|DET WIN STARTX|DET WIN STARTY"

#define HAWKI_HEADER_WCS            "CTYPE1|CTYPE2|CRVAL1|CRVAL2|CRPIX1|CRPIX2|CD1_1|CD1_2|CD2_1|CD2_2"

#define HAWKI_HEADER_COMB_OFFSETS   "ESO QC COMBINED"

#define HAWKI_HEADER_PRI_TOPAF      "^(ARCFILE|MJD-OBS|INSTRUME|ESO TPL ID|ESO TPL NEXP|ESO DPR CATG|ESO DPR TECH|ESO DPR TYPE|DATE-OBS|ESO INS GRAT NAME|ESO INS GRAT WLEN|ESO INS OPTI1 ID|ESO OBS ID|ESO DET MINDIT|ESO DET RSPEED|ESO DET DIT|ESO DET NDIT|ESO DET NCORRS NAME|ESO INS FILT1 NAME|ESO INS FILT2 NAME|ESO MODE NAME|ESO NDSAMPLES)$" 

#define HAWKI_HEADER_EXT_TOPAF      "ESO DET CHIP ID|ESO DET CHIP NO|ESO DET CHIP LIVE"

/*-----------------------------------------------------------------------------
   							        Prototypes
 -----------------------------------------------------------------------------*/

CPL_BEGIN_DECLS
void hawki_print_banner(void);
const char * hawki_get_license_legacy(void);
const char * hawki_get_version(void);

cpl_image * hawki_compute_lsbg
(const cpl_image *     in);
cpl_image * hawki_compute_darkbpm
(const cpl_image *     in,
 double                sigma);
cpl_image * hawki_compute_flatbpm
(const cpl_image *   in,
 double              sigma,
 double              lowval,
 double              highval);
cpl_error_code hawki_image_inverse_threshold
(cpl_image *    image_in,
 double         lo_valid,
 double         hi_valid,
 double         assign_in_range,
 double         assign_out_range);
int hawki_apply_harmonization
(cpl_imagelist *       in,
 double                h1,
 double                h2,
 double                h3,
 double                h4);
int hawki_compute_harmonization
(const cpl_imagelist * in,
 double *              h1,
 double *              h2,
 double *              h3,
 double *              h4,
 double *              h);
const char * hawki_extract_first_filename
(const cpl_frameset *  in,
 const char *          tag);
hawki_band hawki_get_band
(const char *          band);
const char * hawki_std_band_name
(hawki_band            band);
cpl_image * hawki_images_stitch
(cpl_image **    ima,
 double    *     x,
 double    *     y);
cpl_bivector * hawki_get_header_tel_offsets
(const cpl_frameset *  frameset);
double hawki_get_mean_airmass(cpl_frameset    *   set);
cpl_size * hawki_detectors_labelise
(const cpl_frameset *  frameset);
int hawki_detectors_locate_star
(const cpl_frameset * in,
 double               star_ra,
 double               star_dec,
 int                * labels);
int hawki_check_stdstar_alldetectors(int *labels, int nframes);
double hawki_vector_get_min_select
(const cpl_vector * self, const cpl_vector * valid);
double hawki_vector_get_max_select
(const cpl_vector * self, const cpl_vector * valid);
double hawki_vector_get_mode(cpl_vector * vec);
int hawki_utils_check_equal_double_keys
(cpl_frameset * frames, double (*func)(const cpl_propertylist *));
int hawki_utils_check_equal_int_keys
(cpl_frameset * frames, int (*func)(const cpl_propertylist *));
void hawki_utils_ra2str(char * str, int length_str, double ra);
void hawki_utils_dec2str(char * str, int length_str, double dec);
cpl_error_code hawki_frameset_append
(cpl_frameset *self, const cpl_frameset *other);
CPL_END_DECLS

#endif
