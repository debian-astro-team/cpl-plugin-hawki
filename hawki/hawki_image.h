/* $Id: hawki_image.h,v 1.3 2013-03-25 11:35:08 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:08 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_IMAGE_H
#define HAWKI_IMAGE_H

#include "cpl.h"
#include "cpl_image.h"

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

CPL_BEGIN_DECLS
cpl_error_code hawki_image_copy_to_intersection
(cpl_image        * target,
 const cpl_image  * from,
 cpl_size           target_shift_x,
 cpl_size           target_shift_y);
CPL_END_DECLS

#endif
