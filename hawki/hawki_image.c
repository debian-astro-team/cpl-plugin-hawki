/* $Id: hawki_image.c,v 1.4 2013-03-25 11:35:07 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:07 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <math.h>
#include <string.h>
#include <cpl.h>
#include <cpl_mask.h>
#include <cpl_matrix.h>

#include "hawki_mask.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_image   Image manipulation
 *
 */
/*----------------------------------------------------------------------------*/

cpl_error_code hawki_image_copy_to_intersection
(cpl_image        * target,
 const cpl_image  * from,
 cpl_size           target_shift_x,
 cpl_size           target_shift_y);
/**@{*/

/** 
 *   @brief    Copies the values of an image to the intersection of two images.
 *   @param    target         The image where the values will be copied
 *   @param    from           The image from where the values are copied
 *   @param    target_shift_x The shift in pixels between the target image and
 *                            the from image in X direction
 *   @param    target_shift_y The shift in pixels between the target image and
 *                            the from image in Y direction
 * 
 *  This function copies the contents of an image to other image which
 *  is shifted from the first image. The function will copy only the values
 *  of the pixels which belong to the intersection of the two images.
 *  
 */
cpl_error_code hawki_image_copy_to_intersection
(cpl_image        * target,
 const cpl_image  * from,
 cpl_size           target_shift_x,
 cpl_size           target_shift_y)
{
    
    /* Check entries */
    cpl_ensure_code(target       != NULL,      CPL_ERROR_NULL_INPUT);
    cpl_ensure_code(from         != NULL,      CPL_ERROR_NULL_INPUT);
    cpl_ensure_code(cpl_image_get_type(target) == cpl_image_get_type(from),
                    CPL_ERROR_TYPE_MISMATCH);

    cpl_size inter_x1;
    cpl_size inter_x2;
    cpl_size inter_y1;
    cpl_size inter_y2;

    cpl_size from_x1 = 0;
    cpl_size from_x2 = cpl_image_get_size_x(from);
    cpl_size from_y1 = 0;
    cpl_size from_y2 = cpl_image_get_size_y(from);
    
    cpl_size target_x1 = target_shift_x;
    cpl_size target_x2 = target_shift_x + cpl_image_get_size_x(target);
    cpl_size target_y1 = target_shift_y;
    cpl_size target_y2 = target_shift_y + cpl_image_get_size_y(target);
    
    /* Compute intersection */
    inter_x1 = CX_MAX(from_x1, target_x1);
    inter_x2 = CX_MIN(from_x2, target_x2);
    inter_y1 = CX_MAX(from_y1, target_y1);
    inter_y2 = CX_MIN(from_y2, target_y2);
 
    if(inter_x2 > inter_x1 && inter_y2 > inter_y1)
    {
        const void * from_data;
        void       * target_data;
        int          iy;
        size_t       pixel_size = cpl_type_get_sizeof(cpl_image_get_type(from));
        cpl_size     from_nx = cpl_image_get_size_x(from);
        cpl_size     target_nx = cpl_image_get_size_y(target);
        cpl_size     memcopy_size;

        memcopy_size = (inter_x2 - inter_x1) * pixel_size;

        from_data   = cpl_image_get_data_const(from);
        target_data = cpl_image_get_data(target);

        for(iy=inter_y1; iy<inter_y2; ++iy)
        {
            const void * from_p; 
            void       * target_p;
            
            from_p   = (const uint8_t*)from_data   + 
                    (inter_x1 + iy * from_nx) * pixel_size;
            target_p = (uint8_t*)target_data +
                    (inter_x1 - target_shift_x +
                            (iy - target_shift_y) * target_nx) * pixel_size;
            memcpy(target_p, from_p, memcopy_size);
        }
    }
    /* If the if is not executed is because intersection is empty. 
     * Nothing to copy */
    
    return CPL_ERROR_NONE;
}

/**@}*/

