/* $Id: hawki_match_cats.c,v 1.2 2013-03-25 11:35:09 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:09 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <float.h>
#include <string.h>
#include <math.h>
#include <cpl.h>

#include "hawki_dfs_legacy.h"
#include "hawki_utils_legacy.h"
#include "hawki_pfits_legacy.h"
#include "hawki_match_cats.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_obj_det  Functionality related to catalogue matching
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Compare two objects positions and determine whetherthey are within
            a 5 pix tolerance 
  @param    catalogue1  The table with catalogue1
  @param    catalogue2  The table with catalogue2
  @param    iobj1       The position in table catalogue1 of the object to compare
  @param    iobj2       The position in table catalogue2 of the object to compare

  @return   1 if they match, 0 if they do not.
 
  Both tables have to have the columns POS_X and POS_Y, which give the 
  position in the detector in pixels.
  The distance between both objects is the euclidean distance in pixels.
  The object indexes start from 0.
  
  WARNING: Since this function will be called many times, there is no error
  checking. The caller has to ensure that the catalogues are well constructed
  and that the indexes are not out of bounds.

 */
int hawki_match_condition_5_pix
(cpl_table * catalogue1,
 cpl_table * catalogue2,
 int         iobj1,
 int         iobj2)
{
    static const double max_dist2 = 25;

    int                 null;
    double              posx1;
    double              posy1;
    double              posx2;
    double              posy2;
    double              dist2;

    posx1 =
        cpl_table_get_double(catalogue1, HAWKI_COL_OBJ_POSX, iobj1, &null);
    posy1 =
        cpl_table_get_double(catalogue1, HAWKI_COL_OBJ_POSY, iobj1, &null);
    posx2 =
        cpl_table_get_double(catalogue2, HAWKI_COL_OBJ_POSX, iobj2, &null);
    posy2 =
        cpl_table_get_double(catalogue2, HAWKI_COL_OBJ_POSY, iobj2, &null);
    
    dist2 = (posx1 - posx2) * (posx1 - posx2) + (posy1 - posy2) * (posy1 - posy2);
    if(dist2 <= max_dist2)
        return 1;

    return 0;
}

/**@}*/

