/* $Id: hawki_properties_tel.c,v 1.3 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <float.h>
#include <string.h>
#include <math.h>
#include <cpl.h>

#include "hawki_dfs_legacy.h"
#include "hawki_pfits_legacy.h"
#include "hawki_properties_tel.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_properties_tel  Manipulates QC keywords from the telescope 
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Initialize the table with all the telescope data columns 
  @param    telescope_prop     Table with the telescope QC data.
  @return   0 if everything is ok, -1 otherwise

  This function creates the following columns in the given table:
  TEL.ALT, TEL.AZ, TEL.AMBI.RHUM, TEl.AMBI.TAU0, TEL.AMBI.TEMP, 
  TEL.AMBI.WINDDIR, TEL.AMBI.WINDSP, TEL.IA.FWHM,
  SEQ.CUMOFFSETA, SEQ.CUMOFFSETD, SEQ.CUMOFFSETX, SEQ.CUMOFFSETY
  ADA.ABSROT.DELTA, ADA.ABSROT.END, ADA.ABSROT.START
  TEL.AIRM, TEL.AIRM.START, TEL.AIRM.END
  TEL AMBI FWHM, TEL.AMBI.FHWM.START, TEL.AMBI.FWHM.END
  TEL.AMBI.PRES, TEL.AMBI.PRES.START, TEL.AMBI.PRES.END
  TEL.PARANG, TEL.PARANG.START, TEL.PARANG.END
  TEL.PARANG.DELTA, TEL.PARANG.END, TEL.PARANG.START
 */
/*----------------------------------------------------------------------------*/

int hawki_prop_tel_initialize(cpl_table * telescope_prop)
{
    cpl_errorstate  prestate = cpl_errorstate_get();
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_ALT,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AZ,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AIRM,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETA,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETD,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETX,
                         CPL_TYPE_DOUBLE);
    cpl_table_new_column(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETY,
                         CPL_TYPE_DOUBLE);
    if(!cpl_errorstate_is_equal(prestate))
        return -1;
    return 0;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Extracts all the QC keywords and append them to the table 
  @param    all_property_list  The  list of all the properties from the header
  @param    telescope_prop     Table with the telescope QC data.
  @return   0 if everything is ok, -1 otherwise

  The function gets the following keywords from the propertylist: 
  TEL.ALT, TEL.AZ, TEL.AMBI.RHUM, TEl.AMBI.TAU0, TEL.AMBI.TEMP, 
  TEL.AMBI.WINDDIR, TEL.AMBI.WINDSP, TEL.IA.FWHM,
  SEQ.CUMOFFSETA, SEQ.CUMOFFSETD, SEQ.CUMOFFSETX, SEQ.CUMOFFSETY
  It also computes the following extra keywords:
  ADA.ABSROT.DELTA = ADA.ABSROT.END - ADA.ABSROT.START
  TEL.AIRM = (TEL.AIRM.START + TEL.AIRM.END) / 2.0
  TEL AMBI FWHM = (TEL.AMBI.FHWM.START + TEL.AMBI.FWHM.END) / 2.0
  TEL.AMBI.PRES = (TEL.AMBI.PRES.START + TEL.AMBI.PRES.END) / 2.0
  TEL.PARANG = (TEL.PARANG.START + TEL.PARANG.END) / 2.0
  TEL.PARANG.DELTA = TEL.PARANG.END - TEL.PARANG.START
  
  All these keywords (including the *.START and *.END) are appended in the 
  telescope_prop table, using the column names stated in hawki_dfs.h 
 */
/*----------------------------------------------------------------------------*/
int hawki_extract_prop_tel_qc
(const cpl_propertylist * all_property_list,
 cpl_table              * telescope_prop,
 int                      irow)
{
    int ncol;
    int nused;
    double temp_val;
    double temp_start;
    double temp_end;
    cpl_errorstate  prestate;
    
    /* Error check */
    prestate = cpl_errorstate_get();

    /* Checking input */
    if(all_property_list == NULL || telescope_prop == NULL)
    {
        cpl_msg_error(__func__, "The property list or the tel table is null");
        return -1;
    }
    
    /* Checking columns in the table */
    ncol = 0;
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_ALT);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AZ);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AIRM);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_PARANG);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETA);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETD);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETX);
    ncol += cpl_table_has_column(telescope_prop,
                                 HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETY);
    if(ncol != 28)
    {
        cpl_msg_error(__func__,"Table does not have the proper format");
        return -1;
    }
       

    /* Getting and saving the keywords */
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_ALT,
                         irow,
                         hawki_pfits_get_elevation(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AZ,
                         irow,
                         hawki_pfits_get_azimut(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM,
                         irow,
                         hawki_pfits_get_relhum(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0,
                         irow,
                         hawki_pfits_get_tau0(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP,
                         irow,
                         hawki_pfits_get_obs_temp(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR,
                         irow,
                         hawki_pfits_get_wind_dir(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP,
                         irow,
                         hawki_pfits_get_wind_speed(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM,
                         irow,
                         hawki_pfits_get_ao_fwhm(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START,
                         irow,
                         hawki_pfits_get_rotator_start(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END,
                         irow,
                         hawki_pfits_get_rotator_end(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START,
                         irow,
                         hawki_pfits_get_airmass_start(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END,
                         irow,
                         hawki_pfits_get_airmass_end(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START,
                         irow,
                         hawki_pfits_get_obs_seeing_start(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END,
                         irow,
                         hawki_pfits_get_obs_seeing_end(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START,
                         irow,
                         hawki_pfits_get_pressure_start(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END,
                         irow,
                         hawki_pfits_get_pressure_end(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START,
                         irow,
                         hawki_pfits_get_parangle_start(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END,
                         irow,
                         hawki_pfits_get_parangle_end(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETA,
                         irow,
                         hawki_pfits_get_cumoffseta(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETD,
                         irow,
                         hawki_pfits_get_cumoffsetd(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETX,
                         irow,
                         hawki_pfits_get_cumoffsetx(all_property_list));
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_SEQ_CUMOFFSETY,
                         irow,
                         hawki_pfits_get_cumoffsety(all_property_list));
    

    /* Compute and write derived quantities */
    temp_val = hawki_pfits_get_rotator_end(all_property_list) -
               hawki_pfits_get_rotator_start(all_property_list);
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA,
                         irow,
                         temp_val);

    temp_val = (hawki_pfits_get_airmass_start(all_property_list) +
                hawki_pfits_get_airmass_end(all_property_list) ) / 2.0;
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AIRM,
                         irow,
                         temp_val);

    //The "mean" FWHM is computed rejecting one of the values if 
    //is equal to -1. If both are -1, the mean  FWHM is -1.
    temp_start = hawki_pfits_get_obs_seeing_start(all_property_list);
    temp_end = hawki_pfits_get_obs_seeing_end(all_property_list);
    temp_val = 0;
    nused = 0;
    if(temp_start != -1)
    {
        temp_val += temp_start;
        nused++;
    }
    if(temp_end != -1)
    {
        temp_val += temp_end;
        nused++;
    }
    if(nused != 0)
        temp_val = temp_val / nused;
    else
        temp_val = -1;
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM,
                         irow,
                         temp_val);
    
    temp_val = (hawki_pfits_get_pressure_start(all_property_list) +
                hawki_pfits_get_pressure_end(all_property_list) ) / 2.0;
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES,
                         irow,
                         temp_val);
    
    temp_val = (hawki_pfits_get_parangle_start(all_property_list) +
                hawki_pfits_get_parangle_end(all_property_list) ) / 2.0;
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG,
                         irow,
                         temp_val);
    
    temp_val = hawki_pfits_get_rotator_end(all_property_list) -
               hawki_pfits_get_rotator_start(all_property_list);
    cpl_table_set_double(telescope_prop,
                         HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA,
                         irow,
                         temp_val);


    if(!cpl_errorstate_is_equal(prestate))
        return -1;

    return 0;
}

int hawki_compute_prop_tel_qc_stats
(const cpl_table  * telescope_prop,
 cpl_propertylist * stats_prop)
{
    cpl_errorstate  prestate = cpl_errorstate_get();
    /* Checking input */
    if(telescope_prop == NULL || stats_prop == NULL)
    {
        cpl_msg_error(__func__, "The property list or the tel table is null");
        return -1;
    }
    
    /* Compute the statistics */
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL ALT MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_ALT));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL ALT MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_ALT));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL ALT MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_ALT));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL ALT MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_ALT));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL ALT STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_ALT));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AZ MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AZ));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AZ MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AZ));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AZ MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AZ));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AZ MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AZ));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AZ STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AZ));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI RHUM MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI RHUM MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI RHUM MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI RHUM MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI RHUM STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_RHUM));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TAU0 MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TAU0 MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TAU0 MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TAU0 MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TAU0 STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TAU0));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TEMP MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TEMP MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TEMP MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TEMP MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI TEMP STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_TEMP));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDDIR MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDDIR MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDDIR MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDDIR MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDDIR STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDDIR));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDSP MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDSP MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDSP MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDSP MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI WINDSP STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_WINDSP));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL IA FWHM MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL IA FWHM MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL IA FWHM MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL IA FWHM MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL IA FWHM STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_IA_FWHM));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT DELTA MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT DELTA MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT DELTA MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT DELTA MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT DELTA STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_DELTA));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT START MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT START MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT START MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT START MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT START STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_START));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT END MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT END MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT END MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT END MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC ADA ABSROT END STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_ADA_ABSROT_END));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM START MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM START MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM START MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM START MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM START STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_START));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM END MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM END MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM END MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM END MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AIRM END STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AIRM_END));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM START MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM START MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM START MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM START MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM START STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_START));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM END MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM END MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM END MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM END MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI FWHM END STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_FWHM_END));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES START MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES START MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES START MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES START MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES START STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_START));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES END MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES END MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES END MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES END MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL AMBI PRES END STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_AMBI_PRES_END));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG START MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG START MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG START MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG START MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG START STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_START));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG END MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG END MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG END MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG END MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG END STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_END));

    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG DELTA MEAN",cpl_table_get_column_mean
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG DELTA MED",cpl_table_get_column_median
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG DELTA MIN",cpl_table_get_column_min
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG DELTA MAX",cpl_table_get_column_max
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA));
    cpl_propertylist_append_double
      (stats_prop, "ESO QC TEL PARANG DELTA STDEV",cpl_table_get_column_stdev
              (telescope_prop, HAWKI_COL_RAW_JITTER_QC_TEL_PARANG_DELTA));

    if(!cpl_errorstate_is_equal(prestate))
        return -1;
    return 0;
}

/**@}*/

