/* $Id: hawki_pfits.h,v 1.6 2015/08/07 13:07:06 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:06 $
 * $Revision: 1.6 $
 * $Name:  $
 */

/* Includes */

#ifndef HAWKI_PFITS_H
#define HAWKI_PFITS_H

#include <cpl.h>

/* Function prototypes */

extern int hawki_pfits_get_projid(const cpl_propertylist *plist, char *projid);
extern int hawki_pfits_get_dit(const cpl_propertylist *plist, float *dit);
extern int hawki_pfits_get_gain(const cpl_propertylist *plist, float *gain);
extern int hawki_pfits_get_filter(const cpl_propertylist *plist, char *filt);
extern int hawki_pfits_get_ndit(const cpl_propertylist *plist, int *ndit);
extern int hawki_pfits_get_ndsamp(const cpl_propertylist *plist, int *ndit);
extern int hawki_pfits_get_exptime(const cpl_propertylist *plist, 
                                   float *exptime);
extern int hawki_pfits_get_detlive(const cpl_propertylist *plist, int *detlive);
extern int hawki_pfits_get_mjd(const cpl_propertylist *plist, double *mjd);
extern int hawki_pfits_get_tplstart(cpl_propertylist *plist, char *tplstart);
extern int hawki_pfits_get_dateobs(cpl_propertylist *plist, char *dateobs);

#endif

/* 

$Log: hawki_pfits.h,v $
Revision 1.6  2015/08/07 13:07:06  jim
Fixed copyright to ESO

Revision 1.5  2015/01/29 12:00:28  jim
modified comments

Revision 1.4  2014/12/11 12:24:35  jim
Lots of upgrades

Revision 1.3  2013/11/21 09:38:14  jim
detabbed

Revision 1.2  2013-10-24 09:28:44  jim
Added hawki_pfits_get_dateobs

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
