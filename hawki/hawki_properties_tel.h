/* $Id: hawki_properties_tel.h,v 1.2 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_PROPERTIES_TEL_H
#define HAWKI_PROPERTIES_TEL_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>


int hawki_prop_tel_initialize
(cpl_table * telescope_prop);
int hawki_extract_prop_tel_qc
(const cpl_propertylist * all_property_list,
 cpl_table              * telescope_prop,
 int                      irow);
int hawki_compute_prop_tel_qc_stats
(const cpl_table  * telescope_prop,
 cpl_propertylist * stats_prop);

#endif
