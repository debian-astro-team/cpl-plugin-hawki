/* $Id: hawki_dfs.h,v 1.15 2015/08/07 13:07:06 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:06 $
 * $Revision: 1.15 $
 * $Name:  $
 */


/* Includes */

#ifndef HAWKI_DFS_H
#define HAWKI_DFS_H

/*********************************************************/
/*       PRO.CATG keywords for main data products        */
/*********************************************************/

/* Products from hawki_dark_combine */

#define HAWKI_PRO_DARK                "MASTER_DARK"
#define HAWKI_PRO_DIFFIMG_DARK        "DIFFIMG_DARK"
#define HAWKI_PRO_DIFFIMG_DARK_STATS  "DIFFIMG_STATS_DARK"

/* Products from hawki_science_process */

#define HAWKI_PRO_SIMPLE_SCI          "BASIC_CALIBRATED_SCI"
#define HAWKI_PRO_SIMPLE_SKY          "BASIC_CALIBRATED_SKY"
#define HAWKI_PRO_VAR_SCI             "BASIC_VAR_MAP"
#define HAWKI_PRO_VAR_SKY             "BASIC_VAR_MAP_SKY"
#define HAWKI_PRO_JITTERED_SCI        "JITTERED_IMAGE_SCI"
#define HAWKI_PRO_CONF_JITTERED       "CONFIDENCE_MAP_JITTERED"
#define HAWKI_PRO_JITTERED_VAR        "JITTERED_VAR_IMAGE"
#define HAWKI_PRO_OBJCAT_SCI          "OBJECT_CATALOGUE_SCI"
#define HAWKI_PRO_OBJCAT_JITTERED     "OBJECT_CATALOGUE_JITTERED"
#define HAWKI_PRO_MEAN_SKY            "MEAN_SKY"
#define HAWKI_PRO_MEAN_SKY_VAR        "MEAN_SKY_VAR"

/* Product from hawki_standard_process */

#define HAWKI_PRO_SIMPLE_STD          "BASIC_CALIBRATED_STD"
#define HAWKI_PRO_SIMPLE_CAT          "BASIC_CAT_STD"

/* Products from hawki_science_postprocess */

#define HAWKI_PRO_MOSAIC              "TILED_IMAGE"
#define HAWKI_PRO_MOSAIC_CONF         "TILED_CONFIDENCE_MAP"
#define HAWKI_PRO_MOSAIC_VAR          "TILED_VAR_MAP"
#define HAWKI_PRO_MOSAIC_CAT          "TILED_OBJECT_CATALOGUE"

/* Common products from science and standard processing recipes */

#define HAWKI_PRO_MATCHSTD_ASTROM     "MATCHSTD_ASTROM"
#define HAWKI_PRO_MATCHSTD_PHOTOM     "MATCHSTD_PHOTOM"

/* Products from hawki_detector_noise */

#define HAWKI_PRO_READGAIN            "MASTER_READGAIN"

/* Products from hawki_twilight_flat_combine */

#define HAWKI_PRO_TWILIGHT_FLAT                "MASTER_TWILIGHT_FLAT"
#define HAWKI_PRO_CONF                         "MASTER_CONF"
#define HAWKI_PRO_BPM                          "MASTER_BPM"
#define HAWKI_PRO_RATIOIMG_TWILIGHT_FLAT       "RATIOIMG_TWILIGHT_FLAT"
#define HAWKI_PRO_RATIOIMG_TWILIGHT_FLAT_STATS "RATIOIMG_STATS_TWILIGHT_FLAT"

/* Products from hawki_linearity_analyse */

#define HAWKI_LIN_COEFF_CUBE                   "COEFFS_CUBE"
#define HAWKI_LIN_NL_MAP                       "BP_MAP_NL"
#define HAWKI_LIN_TAB                          "DET_LIN_INFO"


/*********************************************************/
/*         DO.CATG keywords for raw data frames          */
/*********************************************************/

/* Raw input for hawki_dark_combine */

#define HAWKI_DARK_RAW                "DARK"

/* Raw input for hawki_science_process */

#define HAWKI_SCI_OBJECT_RAW          "OBJECT"
#define HAWKI_OFFSET_SKY_RAW          "SKY"

/* Raw input for hawki_standard_process */

#define HAWKI_STD_OBJECT_RAW          "STD"

/* Raw input for hawki_twilight_flat_combine */

#define HAWKI_TWI_RAW                 "FLAT_TWILIGHT"

/* Raw input for hawki_linearity_analyse */

#define HAWKI_LIN_FLAT_DETCHECK       "FLAT_LAMP_DETCHECK"
#define HAWKI_LIN_DARK_DETCHECK       "DARK_DETCHECK"

/*********************************************************/
/*     DO.CATG keywords for reference data frames        */
/*     Used for comparison of new master calibrations    */
/*********************************************************/

#define HAWKI_REF_DARK                "REFERENCE_DARK"
#define HAWKI_REF_TWILIGHT_FLAT       "REFERENCE_TWILIGHT_FLAT"
#define HAWKI_REF_BPM                 "REFERENCE_BPM"

/*********************************************************/
/*   DO.CATG keywords for master calibration frames      */
/*********************************************************/

#define HAWKI_CAL_DARK                "MASTER_DARK"
#define HAWKI_CAL_TWILIGHT_FLAT       "MASTER_TWILIGHT_FLAT"
#define HAWKI_CAL_CONF                "MASTER_CONF"
#define HAWKI_CAL_BPM                 "MASTER_BPM"
#define HAWKI_CAL_SKY                 "MASTER_SKY"
#define HAWKI_CAL_SKY_VAR             "MASTER_SKY_VAR"
#define HAWKI_CAL_OBJMASK             "MASTER_OBJMASK"
#define HAWKI_CAL_READGAIN            "MASTER_READGAIN"
#define HAWKI_CAL_MSTD_PHOT           "MATCHSTD_PHOTOM"

/*********************************************************/
/*    DO.CATG keywords for static calibration            */
/*********************************************************/

#define HAWKI_CAL_2MASS_A             "MASTER_2MASS_CATALOGUE_ASTROM"
#define HAWKI_CAL_PPMXL_A             "MASTER_PPMXL_CATALOGUE_ASTROM"
#define HAWKI_CAL_LOCCAT_A            "MASTER_LOCAL_CATALOGUE_ASTROM"
#define HAWKI_CAL_2MASS_P             "MASTER_2MASS_CATALOGUE_PHOTOM"
#define HAWKI_CAL_PPMXL_P             "MASTER_PPMXL_CATALOGUE_PHOTOM"
#define HAWKI_CAL_LOCCAT_P            "MASTER_LOCAL_CATALOGUE_PHOTOM"
#define HAWKI_CAL_PHOTTAB             "PHOTCAL_TAB"
#define HAWKI_CAL_SCHL_N              "SCHLEGEL_MAP_NORTH"
#define HAWKI_CAL_SCHL_S              "SCHLEGEL_MAP_SOUTH"

/* Function prototypes */

extern int hawki_dfs_set_groups(cpl_frameset *);
extern void hawki_dfs_set_product_primary_header(cpl_propertylist *plist,
                                                 cpl_frame *frame,
                                                 cpl_frameset *frameset,
                                                 cpl_parameterlist *parlist,
                                                 const char *recipeid,
                                                 const char *dict,
                                                 cpl_frame *inherit,
                                                 int synch);
extern void hawki_dfs_set_product_exten_header(cpl_propertylist *plist,
                                               cpl_frame *frame,
                                               cpl_frameset *frameset,
                                               cpl_parameterlist *parlist,
                                               const char *recipeid,
                                               const char *dict,
                                               cpl_frame *inherit);
#endif

/*

$Log: hawki_dfs.h,v $
Revision 1.15  2015/08/07 13:07:06  jim
Fixed copyright to ESO

Revision 1.14  2015/06/25 11:56:09  jim
fixed typo for matched standards

Revision 1.13  2015/06/08 09:33:19  jim
changed value for HAWKI_CAL_MSTD_PHOT

Revision 1.12  2015/05/13 11:55:35  jim
Added HAWKI_PRO_VAR_SKY

Revision 1.11  2015/02/17 11:23:08  jim
Added HAWKI_CAL_MSTD_PHOT

Revision 1.10  2015/02/14 12:36:22  jim
Added matched standards product types

Revision 1.9  2015/01/29 11:58:00  jim
Added master readgain product

Revision 1.8  2014/12/12 21:45:16  jim
removed inverse variance stuff

Revision 1.7  2014/12/11 12:24:35  jim
Lots of upgrades

Revision 1.6  2014/05/29 11:03:44  jim
Modified to create new product tags for jittered confidence maps and
catalogues as well as tile catalogues

Revision 1.5  2014/04/27 10:10:56  jim
Modified to add in local standard star catalogues

Revision 1.4  2014/03/26 16:06:32  jim
Added HAWKI_CAL_READGAIN

Revision 1.3  2013/11/21 09:38:14  jim
detabbed

Revision 1.2  2013/11/07 10:04:53  jim
removed spurious definitions

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
