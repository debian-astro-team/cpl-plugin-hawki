/* $Id: hawki_fwhm.c,v 1.5 2015/08/07 13:07:06 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2015/08/07 13:07:06 $
 * $Revision: 1.5 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <string.h>
#include "hawki_fwhm.h"

/**
    \defgroup hawki_fwhm hawki_fwhm
    \ingroup supportroutines

    \brief
    These are routines to compute the fwhm

    \author
    Cesar Enrique Garcia, ESO
*/

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        hawki_get_dimm_fwhm
    \par Purpose:
        Use the information in the header to get the FWHM as measured by the
         DIMM monitor and corrected to the bandpass of HAWK-I.
    \par Description:
        Use the information in the header to get the FWHM as measured by the
         DIMM monitor and corrected to the bandpass of HAWK-I.
    \par Language:
        C
    \param plist
        The header with the relevant keywords
    \returns
        The DIMM FWHM for the HAWK-I bandpass
    \author
        Cesar Enrique Garcia, ESO
 */
/*---------------------------------------------------------------------------*/
double hawki_get_dimm_fwhm(cpl_propertylist * plist)
{
    double airmass_start, airmass_end, dimm_fwhm_start, dimm_fwhm_end;
    double airmass_mean, dimm_fwhm_mean, filter_wave, dimm_fwhm;
    const char * filter_name;
    airmass_start   = cpl_propertylist_get_double(plist, "ESO TEL AIRM START");
    airmass_end     = cpl_propertylist_get_double(plist, "ESO TEL AIRM END");
    dimm_fwhm_start = cpl_propertylist_get_double(plist, 
                                                  "ESO TEL AMBI FWHM START");
    dimm_fwhm_end   = cpl_propertylist_get_double(plist, 
                                                  "ESO TEL AMBI FWHM END");
    airmass_mean   = (airmass_start + airmass_end) / 2.;
    dimm_fwhm_mean = (dimm_fwhm_start + dimm_fwhm_end) / 2.;
    if(dimm_fwhm_start < 0 || dimm_fwhm_end < 0)
        return -1;
    filter_name = cpl_propertylist_get_string(plist, "ESO INS FILT1 NAME");
    filter_wave = hawki_get_filter_central_wave(filter_name);
    dimm_fwhm = hawki_get_fwhm_dimm_filter(dimm_fwhm_mean, filter_wave,
                                           airmass_mean);

    if (cpl_error_get_code() != CPL_ERROR_NONE)
        return 0;
    return dimm_fwhm;
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        hawki_get_fwhm_dimm_filter
    \par Purpose:
        Compute the FWHM for the HAWK-I band in use from the DIMM FWHM
    \par Description:
        Given the DIMM FWHM which is computed at the DIMM reference wavelength
        (500 nm), this function computes the equivalent FWHM at the band
        observed by HAWK-I.
         The function used to compute the DIMM seeing uses the relationship 
         described by Sarazin (2008 Messenger 132, pg.11) and used in
         http://www.eso.org/observing/dfo/quality/GENERAL/IQ/ImageQuality.html 
    \par Language:
        C
    \param fwhm_dimm
        FWHM as measured by the DIMM (arcsec) 
    \param wlength
        HAWK-I wavelength in microns 
    \param airmass
        Average airmass of the observation
    \returns
        The HAWK-I FWHM estimation from the DIMM monitor
    \author
        Cesar Enrique Garcia, ESO
 */
/*---------------------------------------------------------------------------*/
double hawki_get_fwhm_dimm_filter(double fwhm_dimm, double wlength, 
                                  double airmass)
{

    double k; //This is the "outer scale correction" for a pupil of 4m diameter
    double wlength_ums = wlength/1000.0; //In microns
    double wlength_dimm = 0.5; //Wavelength of the DIMM in microns

    k = sqrt(1. - 78.08 * (pow(wlength_ums*1.e-6,0.4) * pow(airmass,-0.6) * 
             pow(fwhm_dimm, (-1./3.))));
    double fwhm_ins = (fwhm_dimm * pow(wlength_dimm,0.2) * pow(airmass,0.6) * k)
                      / wlength_ums;

    return fwhm_ins;
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        hawki_get_filter_central_wave
    \par Purpose:
        Get the approximate central wavelength that correspondos to each 
         of the HAWK-I filters
    \par Description:
        Given a HAWK-I filter, the function returns the central wavelength
         of the filter.
    \par Language:
        C
    \param filter
        The filter used
    \returns
        The central wavelength
    \author
        Cesar Enrique Garcia, ESO
 */
/*---------------------------------------------------------------------------*/
double hawki_get_filter_central_wave(const char * filter)
{

    if (!strcmp(filter, "J"))       return 1258.0;
    if (!strcmp(filter, "H"))       return 1620.0;
    if (!strcmp(filter, "Ks"))      return 2146.0;
    if (!strcmp(filter, "Y"))       return 1021.0;
    if (!strcmp(filter, "CH4"))     return 1575.0;
    if (!strcmp(filter, "BrG"))     return 2165.0;
    if (!strcmp(filter, "H2"))      return 2124.0;
    if (!strcmp(filter, "NB0984"))  return 983.7;
    if (!strcmp(filter, "NB1060"))  return 1061.0;
    if (!strcmp(filter, "NB1190"))  return 1186.0;
    if (!strcmp(filter, "NB2090"))  return 2095.0;

    return 0;
}
