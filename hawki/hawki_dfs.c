/* $Id: hawki_dfs.c,v 1.15 2015/11/25 10:28:30 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/11/25 10:28:30 $
 * $Revision: 1.15 $
 * $Name:  $
 */


/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <cpl.h>

#include "hawki_dfs.h"
#include "hawki_utils.h"
#include "casu_utils.h"

#define PACKSZ 1024
#define DELEXTN "^[:blank:]*(ESO.INS |ESO.OBS |ESO.TEL |ESO.PRO |INSTRUME|PIPEFILE|ORIGIN" \
                "|TELESCOP|OBJECT|RA|DEC|EPOCH|EQUINOX|DATAMD5|RADECSYS" \
                "|DATE-OBS|MJD-OBS|UTC|LST|PI-COI|OBSERVER).*$"

#define REGFIX "^(ORIGIN|TELESCOP|INSTRUME|OBJECT|RA|DEC|EPOCH|EQUINOX" \
    "|RADECSYS|DATE-OBS|MJD-OBS|UTC|LST|PI-COI|OBSERVER)$"

/**
   \defgroup hawki_dfs hawki_dfs
   \ingroup supportroutines

   \brief
   These are methods for manipulating keywords in data product headers

   \author
   Jim Lewis, CASU

*/

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        hawki_dfs_set_groups
    \par Purpose:
        Set the group of each frame in a frameset as RAW or CALIB according
        to DO.CATG tag given to it.
    \par Description:
        A frameset is given. Each frame in the frameset is classified as
        RAW or CALIB depending upon the dfs tag it's been given.
    \par Language:
        C
    \param set     
        The input frameset
    \retval CASU_OK 
        If everything is ok
    \retval CASU_FATAL 
        If NULL frameset
    \author
        Jim Lewis, CASU

 */
/*---------------------------------------------------------------------------*/

extern int hawki_dfs_set_groups(cpl_frameset *set) {
    cpl_frame *cur_frame;
    const char *tag;
    const char *fctid = "hawki_dfs_set_groups";
    int nframes,i;

    /* Check entries */

    if (set == NULL) 
        return(CASU_FATAL); 
    
    /* Initialize */

    nframes = cpl_frameset_get_size(set);

    /* Loop on frames */

    for (i = 0; i < nframes; i++) {
        cur_frame = cpl_frameset_get_position(set,i);
        tag = cpl_frame_get_tag(cur_frame);
       
        /* RAW frames */

        if (!strcmp(tag,HAWKI_DARK_RAW)  ||
            !strcmp(tag,HAWKI_SCI_OBJECT_RAW) || 
            !strcmp(tag,HAWKI_STD_OBJECT_RAW) || 
            !strcmp(tag,HAWKI_OFFSET_SKY_RAW) ||
            !strcmp(tag,HAWKI_TWI_RAW) ||
            !strcmp(tag,HAWKI_LIN_FLAT_DETCHECK) ||
            !strcmp(tag,HAWKI_LIN_DARK_DETCHECK) ||
            !strcmp(tag,HAWKI_PRO_SIMPLE_SCI) ||
            !strcmp(tag,HAWKI_PRO_VAR_SCI) ||
            !strcmp(tag,HAWKI_PRO_OBJCAT_SCI) ||
            !strcmp(tag,HAWKI_PRO_CONF_JITTERED) ||
            !strcmp(tag,HAWKI_PRO_OBJCAT_JITTERED))
            cpl_frame_set_group(cur_frame,CPL_FRAME_GROUP_RAW);

        /* CALIB frames */

        else if (!strcmp(tag,HAWKI_CAL_DARK) ||
                 !strcmp(tag,HAWKI_CAL_TWILIGHT_FLAT) ||
                 !strcmp(tag,HAWKI_CAL_CONF) ||
                 !strcmp(tag,HAWKI_CAL_BPM) ||
                 !strcmp(tag,HAWKI_CAL_SKY) ||
                 !strcmp(tag,HAWKI_CAL_SKY_VAR) ||
                 !strcmp(tag,HAWKI_CAL_OBJMASK) ||
                 !strcmp(tag,HAWKI_CAL_2MASS_A) ||
                 !strcmp(tag,HAWKI_CAL_PPMXL_A) ||
                 !strcmp(tag,HAWKI_CAL_LOCCAT_A) ||
                 !strcmp(tag,HAWKI_CAL_2MASS_P) ||
                 !strcmp(tag,HAWKI_CAL_PPMXL_P) ||
                 !strcmp(tag,HAWKI_CAL_LOCCAT_P) ||
                 !strcmp(tag,HAWKI_CAL_PHOTTAB) ||
                 !strcmp(tag,HAWKI_CAL_SCHL_N) ||
                 !strcmp(tag,HAWKI_CAL_SCHL_S) ||
                 !strcmp(tag,HAWKI_CAL_READGAIN) ||
                 !strcmp(tag,HAWKI_CAL_MSTD_PHOT) ||
                 !strcmp(tag,HAWKI_REF_DARK) ||
                 !strcmp(tag,HAWKI_REF_BPM) ||
                 !strcmp(tag,HAWKI_REF_TWILIGHT_FLAT))
            cpl_frame_set_group(cur_frame,CPL_FRAME_GROUP_CALIB);
        else 
            cpl_msg_info(fctid,"No such tag as %s in frame %s",tag,
                         cpl_frame_get_filename(cur_frame));
    }
    return(CASU_OK);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        hawki_dfs_set_product_primary_header
    \par Purpose:
        Set the dfs product header keywords in the primary header
    \par Description:
        Set the dfs product header keywords in the primary header
    \par Language:
        C
    \param plist     
        The propertylist to which the keywords should be appended
    \param frame
        The output product frame
    \param frameset
        The input recipe frameset
    \param parlist
        The parameter list of the input recipe
    \param recipeid
        The name of the input recipe
    \param dict
        The dictionary ID
    \param inherit
        The frame from which you want to inherit useful header information
    \param synch
        If set, then the timing information that has been added by the
        relevant cpl_ routine can be overwritten by the timing information
        from the original propertylist
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void hawki_dfs_set_product_primary_header(cpl_propertylist *plist,
                                                 cpl_frame *frame,
                                                 cpl_frameset *frameset,
                                                 cpl_parameterlist *parlist,
                                                 const char *recipeid, 
                                                 const char *dict,
                                                 cpl_frame *inherit,
                                                 int synch) {
    const char *fctid="hawki_dfs_set_product_primary_header";
    char package[PACKSZ],key1[32],key2[32];
    const char *fn,*tp;
    cpl_propertylist *pcopy;
    int nraw,i,np;

    /* Get the package name and version */

    (void)snprintf(package,PACKSZ,"%s/%s",PACKAGE,PACKAGE_VERSION);

    /* Make a copy of the input propertylist so that we can restore the 
       time and position dependent headers back to the way they were before
       cpl_dfs_setup_product_header worked on it. */

    pcopy = cpl_propertylist_duplicate(plist);

    /* Add the data-flow keywords */

    if (cpl_dfs_setup_product_header(plist,frame,frameset,parlist,recipeid,
                                     package,dict,inherit) != CPL_ERROR_NONE) {
        cpl_msg_warning(fctid,
                        "Problem with the product primary DFS compliance -- %s",
                        cpl_error_get_message());
        cpl_error_reset();
    }

    /* Now fix the time/position information back to the way it was */

    if (synch) 
        cpl_propertylist_copy_property_regexp(plist,
                                              (const cpl_propertylist *)pcopy,
                                              REGFIX,0);

    /* Fix the provenance to point to the raw files */

    nraw = cpl_propertylist_get_int(plist,"ESO PRO DATANCOM");
    cpl_propertylist_erase_regexp(plist,"PROV[0-9]*",0);
    np = 0;
    for (i = 1; i <= nraw; i++) {
        (void)snprintf(key1,32,"ESO PRO REC1 RAW%d NAME",i);
        (void)snprintf(key2,32,"ESO PRO REC1 RAW%d CATG",i);
        fn = cpl_propertylist_get_string(plist,key1);
        tp = cpl_propertylist_get_string(plist,key2);
        if (strcmp(tp,HAWKI_SCI_OBJECT_RAW) == 0) {
            np++;
            (void)snprintf(key1,32,"PROV%d",np);
            cpl_propertylist_update_string(plist,key1,fn);
        }
    }
    cpl_propertylist_delete(pcopy);
    cpl_propertylist_update_string(plist,"PROCSOFT",PACKAGE_VERSION);
    // PIPE-9888
    cpl_propertylist_erase_regexp(plist, "^[:blank:]*RADECSYS[:blank:]*$", 0);
}

/*---------------------------------------------------------------------------*/
/**
    \par Name:
        hawki_dfs_set_product_exten_header
    \par Purpose:
        Set the dfs product header keywords in the primary header
    \par Description:
        Set the dfs product header keywords in the primary header
    \par Language:
        C
    \param plist     
        The propertylist to which the keywords should be appended
    \param frame
        The output product frame
    \param frameset
        The input recipe frameset
    \param parlist
        The parameter list of the input recipe
    \param recipeid
        The name of the input recipe
    \param dict
        The dictionary ID
    \param inherit
        The frame from which you want to inherit all the useful header info
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void hawki_dfs_set_product_exten_header(cpl_propertylist *plist,
                                               cpl_frame *frame,
                                               cpl_frameset *frameset,
                                               cpl_parameterlist *parlist,
                                               const char *recipeid, 
                                               const char *dict,
                                               cpl_frame *inherit) {
    const char *fctid="hawki_dfs_set_product_exten_header";
    char package[PACKSZ];

    /* Get the package name and version */

    (void)snprintf(package,PACKSZ,"%s/%s",PACKAGE,PACKAGE_VERSION);

    /* Add the data-flow keywords */

    if (cpl_dfs_setup_product_header(plist,frame,frameset,parlist,recipeid,
                                     package,dict,inherit) != CPL_ERROR_NONE) {
        cpl_msg_warning(fctid,
                        "Problem with the product extn DFS compliance -- %s",
                        cpl_error_get_message());
        cpl_error_reset();
    }

    /* Remove the stuff that isn't supposed to be here */

    cpl_propertylist_erase_regexp(plist,DELEXTN,0);

}

/**@}*/

/*

$Log: hawki_dfs.c,v $
Revision 1.15  2015/11/25 10:28:30  jim
removes ESO.PRO from extension headers

Revision 1.14  2015/11/16 12:40:29  jim
Added PROCSOFT to primary header

Revision 1.13  2015/10/15 11:21:43  jim
Provenance now points to raw files...

Revision 1.12  2015/08/07 13:07:06  jim
Fixed copyright to ESO

Revision 1.11  2015/02/17 11:23:07  jim
Added HAWKI_CAL_MSTD_PHOT

Revision 1.10  2015/01/29 12:00:28  jim
modified comments

Revision 1.9  2014/12/12 21:45:16  jim
removed inverse variance stuff

Revision 1.8  2014/12/11 12:24:35  jim
Lots of upgrades

Revision 1.7  2014/05/29 11:03:44  jim
Modified to create new product tags for jittered confidence maps and
catalogues as well as tile catalogues

Revision 1.6  2014/04/27 10:10:56  jim
Modified to add in local standard star catalogues

Revision 1.5  2014/03/26 16:06:32  jim
Added HAWKI_CAL_READGAIN

Revision 1.4  2013/11/21 09:38:14  jim
detabbed

Revision 1.3  2013/11/07 10:04:53  jim
removed spurious definitions

Revision 1.2  2013-09-30 18:07:14  jim
Modified to add stack products to list of raw types as they are used in
hawki_science_postprocess

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
