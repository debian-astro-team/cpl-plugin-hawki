/* $Id: hawki_image_stats.h,v 1.5 2013-03-25 11:35:08 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:08 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_IMAGE_STATS_H
#define HAWKI_IMAGE_STATS_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>


int hawki_image_stats_initialize
(cpl_table ** raw_stats);
int hawki_image_stats_fill_from_image
(cpl_table       ** image_stats,
 const cpl_image *  image,
 int                llx,
 int                lly,
 int                urx,
 int                ury,
 int                idet,
 int                irow);
int hawki_image_stats_odd_even_column_row_fill_from_image
(cpl_table       ** odd_column_stats,
 cpl_table       ** even_column_stats,
 cpl_table       ** odd_row_stats,
 cpl_table       ** even_row_stats,
 const cpl_image *  image,
 int                idet,
 int                irow);
int hawki_image_stats_fill_from_frame
(cpl_table       ** image_stats,
 const cpl_frame *  frame,
 int                irow);
int hawki_image_stats_print
(cpl_table ** table_stats);
int hawki_image_stats_stats
(cpl_table         ** image_stats,
 cpl_propertylist  ** stats_stats);
double hawki_image_float_get_sigma_from_quartile(cpl_image * image);
#endif
