/* $Id: hawki_saa.h,v 1.6 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_SAA_H
#define HAWKI_SAA_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
   							        Prototypes
 -----------------------------------------------------------------------------*/

int hawki_geom_refine_images_offsets
(cpl_imagelist  *  in,
 cpl_bivector   *  approx_offsets,
 cpl_bivector   *  reference_objects,
 int               s_hx,
 int               s_hy,
 int               m_hx,
 int               m_hy,
 cpl_bivector   *  refined_offsets,
 cpl_vector     *  correl);

#endif
