/* $Id: hawki_distortion.h,v 1.14 2013-03-25 11:35:07 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:07 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_DISTORTION_H
#define HAWKI_DISTORTION_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                    Structures and typedefs
 -----------------------------------------------------------------------------*/

typedef struct _hawki_distortion_ hawki_distortion;

struct _hawki_distortion_
{
    cpl_image   * dist_x; 
    cpl_image   * dist_y;
    double        x_crval;
    double        x_cdelt;
    double        y_crval;
    double        y_cdelt;
};

/*-----------------------------------------------------------------------------
   							        Prototypes
 -----------------------------------------------------------------------------*/

CPL_BEGIN_DECLS
hawki_distortion * hawki_distortion_grid_new
(int detector_nx, 
 int detector_ny, 
 int grid_size);

void hawki_distortion_delete
(hawki_distortion * distortion);

hawki_distortion * hawki_distortion_load
(const cpl_frame * dist_x,
 const cpl_frame * dist_y,
 int               idet);

int hawki_distortion_get_size_x
(const hawki_distortion * distortion);

int hawki_distortion_get_size_y
(const hawki_distortion * distortion);

int hawki_distortion_correct_alldetectors
(cpl_image       ** alldetectors,
 const cpl_frame  * distortion_x,
 const cpl_frame  * distortion_y) ;

cpl_image * hawki_distortion_correct_detector
(cpl_image       *  image,
 cpl_image       *  dist_x,
 cpl_image       *  dist_y);

int hawki_distortion_correct_coords
(const hawki_distortion * distortion, 
 double                   x_pos,
 double                   y_pos,
 double                *  x_pos_distcorr, 
 double                *  y_pos_distcorr);

int hawki_distortion_inverse_correct_coords
(const hawki_distortion * distortion, 
 double                   x_pos,
 double                   y_pos,
 double                *  x_pos_distdecorr, 
 double                *  y_pos_distdecorr);

int hawki_distortion_apply_maps
(cpl_imagelist   *  ilist, 
 cpl_image       ** dist_x,
 cpl_image       ** dist_y);

int hawki_distortion_create_maps_detector
(const hawki_distortion * distortion, 
 cpl_image              * dist_detector_x,
 cpl_image              * dist_detector_y);

int hawki_distortion_create_maps
(const hawki_distortion * distortion, 
 cpl_image            **  dist_x,
 cpl_image            **  dist_y);

hawki_distortion * hawki_distortion_compute_solution
(const cpl_table       ** ref_catalogues,
 const cpl_bivector     * cat_offsets,
 const cpl_table        * matching_sets,
 int                      ncats,
 int                      detector_nx,
 int                      detector_ny,
 int                      grid_size,
 const hawki_distortion * dist_initguess,
 double                 * rms);
CPL_END_DECLS

#endif
