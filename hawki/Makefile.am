## Process this file with automake to produce Makefile.in

##   This file is part of the HAWKI Pipeline
##   Copyright (C) 2002,2003 European Southern Observatory

##   This library is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation; either version 2 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##   You should have received a copy of the GNU General Public License
##   along with this program; if not, write to the Free Software
##   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

AUTOMAKE_OPTIONS = 1.8 foreign

DISTCLEANFILES = *~

SUBDIRS = . tests

if MAINTAINER_MODE

MAINTAINERCLEANFILES = Makefile.in

endif


AM_CPPFLAGS = -DCX_LOG_DOMAIN=\"HawkiLib\"  $(all_includes) $(GSL_INCLUDES)

AM_CFLAGS = $(GSL_CFLAGS) $(OPENMP_CFLAGS)


noinst_HEADERS =    hawki_utils.h \
                    hawki_utils_legacy.h \
                    hawki_alloc.h \
                    hawki_pfits.h \
                    hawki_pfits_legacy.h \
                    hawki_dfs.h \
                    hawki_dfs_legacy.h \
                    hawki_image.h \
                    hawki_fwhm.h \
                    hawki_load.h \
                    hawki_save.h \
                    hawki_calib.h \
                    hawki_properties_tel.h \
                    hawki_obj_det.h \
                    hawki_image_stats.h \
                    hawki_bkg.h \
                    hawki_saa.h \
                    hawki_match_cats.h \
                    hawki_mask.h \
                    hawki_combine.h \
                    hawki_distortion.h \
                    hawki_var.h \
                    hawki_variance.h 

pkginclude_HEADERS = 

privatelib_LTLIBRARIES = libhawki.la

libhawki_la_SOURCES =      hawki_utils.c \
                           hawki_utils_legacy.c \
                           hawki_alloc.c \
                           hawki_pfits.c \
                           hawki_pfits_legacy.c \
                           hawki_dfs.c \
                           hawki_dfs_legacy.c \
                           hawki_image.c \
                           hawki_fwhm.c \
                           hawki_load.c \
                           hawki_save.c \
                           hawki_calib.c \
                           hawki_properties_tel.c \
                           hawki_obj_det.c \
                           hawki_image_stats.c \
                           hawki_bkg.cc \
                           hawki_match_cats.c \
                           hawki_mask.c \
                           hawki_saa.c \
                           hawki_combine.c \
                           hawki_distortion.c \
                           hawki_var.c \
                           hawki_variance.c

libhawki_la_LDFLAGS = $(EXTRA_LDFLAGS) $(CPL_LDFLAGS) -version-info $(LT_CURRENT):$(LT_REVISION):$(LT_AGE)
libhawki_la_LIBADD = $(LIBCASU) $(LIBCASUCAT) $(LIBIRPLIB) $(LIBCPLDFS) $(LIBCPLUI) $(LIBCPLDRS) $(LIBCPLCORE)  $(GSL_LIBS)
libhawki_la_DEPENDENCIES = $(LIBIRPLIB) $(LIBCASU) $(LIBCASUCAT)
