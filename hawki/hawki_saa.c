/* $Id: hawki_saa.c,v 1.11 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.11 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <float.h>
#include <string.h>
#include <math.h>
#include <cpl.h>

#include "hawki_saa.h"

/*----------------------------------------------------------------------------*/
/**
  @brief    Refine the offsets using cross-correlation of images.
  @param    in                the images with the objects
  @param    approx_offsets    the approximated offsets
  @param    reference_objects the positions of the objects to cross-correlate 
  @param    refined_offsets   the result of the refinement.  
  @return   0 if everything OK. != 0 elsewhere.
 */
/*----------------------------------------------------------------------------*/
int hawki_geom_refine_images_offsets
(cpl_imagelist  *  in,
 cpl_bivector   *  approx_offsets,
 cpl_bivector   *  reference_objects,
 int               s_hx,
 int               s_hy,
 int               m_hx,
 int               m_hy,
 cpl_bivector   *  refined_offsets,
 cpl_vector     *  correl)
{
    cpl_bivector        *   offs_ref ;
    double              *   offs_ref_x ;
    double              *   offs_ref_y ;
    double              *   correl_data ;
    int                     nimages;
    int                     ngood;
    int                     i;

    /* Check entries */
    if (approx_offsets  == NULL) return -1;
    if (refined_offsets == NULL) return -1;

    /* Get the number of images */
    nimages = cpl_imagelist_get_size(in) ;
    if (cpl_bivector_get_size(approx_offsets) != nimages) 
    {
        cpl_msg_error(__func__, "Invalid input objects sizes") ; 
        return -1;
    }
    
    /* Refine the offsets */
    cpl_msg_info(__func__, "Refine the offsets") ;
    cpl_msg_indent_more() ;
    if ((offs_ref = cpl_geom_img_offset_fine
            (in, approx_offsets, reference_objects,
             s_hx, s_hy, m_hx, m_hy, correl)) == NULL)
    {
        cpl_msg_error(cpl_func, "Cannot refine the offsets");
        cpl_vector_delete(correl) ;
        return -1;
    }

    /* Display the results */
    offs_ref_x = cpl_bivector_get_x_data(offs_ref) ;
    offs_ref_y = cpl_bivector_get_y_data(offs_ref) ;
    correl_data = cpl_vector_get_data(correl) ;
    cpl_msg_info(cpl_func, "Refined relative offsets [correlation factor]") ;
    ngood = 0 ;
    for (i=0 ; i<nimages ; i++) 
    {
        cpl_msg_info(cpl_func, "#%02d: %8.2f %8.2f [%12.2f]",
                     i+1, offs_ref_x[i], offs_ref_y[i], correl_data[i]) ;
        if (correl_data[i] > -0.5) ngood++ ;
    }
    if (ngood == 0) 
    {
        cpl_msg_error(__func__, "No frame correctly correlated") ;
        cpl_bivector_delete(offs_ref) ;
        cpl_vector_delete(correl) ;
        return -1;
    }
    cpl_msg_indent_less();

    /* Copy the result */
    cpl_vector_copy(cpl_bivector_get_x(refined_offsets),
                    cpl_bivector_get_x(offs_ref));
    cpl_vector_copy(cpl_bivector_get_y(refined_offsets),
                    cpl_bivector_get_y(offs_ref));

    /* Free and return */
    cpl_bivector_delete(offs_ref);
    cpl_msg_indent_less() ;
    return 0;
}
