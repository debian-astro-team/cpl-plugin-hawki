/* $Id: hawki_pfits.h,v 1.17 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.17 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_PFITS_H
#define HAWKI_PFITS_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
   								Functions prototypes
 -----------------------------------------------------------------------------*/

CPL_BEGIN_DECLS
const char * hawki_pfits_get_extname(const cpl_propertylist * plist) ;
double hawki_pfits_get_airmass_start(const cpl_propertylist * plist) ;
double hawki_pfits_get_airmass_end(const cpl_propertylist * plist) ;
double hawki_pfits_get_humidity_level(const cpl_propertylist * plist) ;
double hawki_pfits_get_targ_alpha(const cpl_propertylist * plist) ;
double hawki_pfits_get_targ_alpha_hhmmss(const cpl_propertylist * plist) ;
double hawki_pfits_get_targ_delta(const cpl_propertylist * plist) ;
double hawki_pfits_get_targ_delta_ddmmss(const cpl_propertylist * plist) ;
double hawki_pfits_get_targ_equinox(const cpl_propertylist * plist);
double hawki_pfits_get_targ_epoch(const cpl_propertylist * plist);
double hawki_pfits_get_ra(const cpl_propertylist * plist) ;
double hawki_pfits_get_dec(const cpl_propertylist * plist) ;
const char * hawki_pfits_get_arcfile(const cpl_propertylist * plist) ;
const char * hawki_pfits_get_procatg(const cpl_propertylist * plist) ;
const char * hawki_pfits_get_date_obs(const cpl_propertylist * plist) ;
int hawki_pfits_get_naxis1(const cpl_propertylist * plist) ;
int hawki_pfits_get_naxis2(const cpl_propertylist * plist) ;
double hawki_pfits_get_crval1(const cpl_propertylist * plist) ;
double hawki_pfits_get_crval2(const cpl_propertylist * plist) ;
double hawki_pfits_get_crpix1(const cpl_propertylist * plist) ;
double hawki_pfits_get_crpix2(const cpl_propertylist * plist) ;
double hawki_pfits_get_cdelta1(const cpl_propertylist * plist) ;
double hawki_pfits_get_cdelta2(const cpl_propertylist * plist) ;
double hawki_pfits_get_pixscale(const cpl_propertylist * plist) ;
double hawki_pfits_get_cumoffseta(const cpl_propertylist * plist);
double hawki_pfits_get_cumoffsetd(const cpl_propertylist * plist);
double hawki_pfits_get_cumoffsetx(const cpl_propertylist * plist);
double hawki_pfits_get_cumoffsety(const cpl_propertylist * plist);
double hawki_pfits_get_posangle(const cpl_propertylist * plist) ;
int hawki_pfits_get_nexp(const cpl_propertylist * plist) ;
int hawki_pfits_get_ndit_legacy(const cpl_propertylist * plist) ;
int hawki_pfits_get_ndsamples(const cpl_propertylist * plist);
double hawki_pfits_get_dit_legacy(const cpl_propertylist * plist) ;
const char * hawki_pfits_get_filter_legacy(const cpl_propertylist * plist) ;
double hawki_pfits_get_focus(const cpl_propertylist * plist) ;
double hawki_pfits_get_elevation(const cpl_propertylist * plist);
double hawki_pfits_get_azimut(const cpl_propertylist * plist);
double hawki_pfits_get_relhum(const cpl_propertylist * plist);
double hawki_pfits_get_tau0(const cpl_propertylist * plist);
double hawki_pfits_get_obs_temp(const cpl_propertylist * plist);
double hawki_pfits_get_wind_dir(const cpl_propertylist * plist);
double hawki_pfits_get_wind_speed(const cpl_propertylist * plist);
double hawki_pfits_get_ao_fwhm(const cpl_propertylist * plist);
double hawki_pfits_get_rotator_start(const cpl_propertylist * plist);
double hawki_pfits_get_rotator_end(const cpl_propertylist * plist);
double hawki_pfits_get_obs_seeing_start(const cpl_propertylist * plist);
double hawki_pfits_get_obs_seeing_end(const cpl_propertylist * plist);
double hawki_pfits_get_pressure_start(const cpl_propertylist * plist);
double hawki_pfits_get_pressure_end(const cpl_propertylist * plist);
double hawki_pfits_get_parangle_start(const cpl_propertylist * plist);
double hawki_pfits_get_parangle_end(const cpl_propertylist * plist);
double hawki_pfits_get_comb_cumoffsetx(const cpl_propertylist * plist);
double hawki_pfits_get_comb_cumoffsety(const cpl_propertylist * plist);
double hawki_pfits_get_comb_posx(const cpl_propertylist * plist);
double hawki_pfits_get_comb_posy(const cpl_propertylist * plist);
CPL_END_DECLS

#endif
