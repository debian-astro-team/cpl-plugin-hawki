/* $Id: hawki_pfits.c,v 1.6 2015/08/07 13:07:06 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:07:06 $
 * $Revision: 1.6 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include <string.h>

#include "casu_utils.h"
#include "hawki_pfits.h"

static int hawki_pfits_get_float(const cpl_propertylist *plist, 
                                 const char *key, float *fval);
static int hawki_pfits_get_double(const cpl_propertylist *plist, 
                                  const char *key, double *fval);

/**
   \defgroup hawki_pfits hawki_pfits
   \ingroup supportroutines

   \brief
   These are support routines used for extracting information from FITS 
   headers.

   \author Jim Lewis, CASU
*/

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the project id
    \param    plist       property list to read from
    \param    projid      requested name of the project
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_projid(const cpl_propertylist *plist, char *projid) {

    strcpy(projid,cpl_propertylist_get_string(plist,"ESO OBS PROG ID"));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of DIT
    \param    plist       property list to read from
    \param    dit         requested value of dit
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_dit(const cpl_propertylist *plist, float *dit) {

    return(hawki_pfits_get_float(plist,"ESO DET DIT",dit));
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of the gain for a detector
    \param    plist       property list to read from
    \param    dit         requested value of dit
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_gain(const cpl_propertylist *plist, float *gain) {

    return(hawki_pfits_get_float(plist,"GAIN",gain));
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the name of the current filter.
    \param    plist       property list to read from
    \param    filt        requested name of the filter
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_filter(const cpl_propertylist *plist, char *filt) {
    char filt1[16],filt2[16];

    if(cpl_propertylist_has(plist,"ESO INS FILT1 NAME") == 0 ||
                    cpl_propertylist_has(plist,"ESO INS FILT2 NAME") == 0) {
        return(CASU_FATAL);
    }

    cpl_errorstate prestate = cpl_errorstate_get();

    strncpy(filt1,cpl_propertylist_get_string(plist,"ESO INS FILT1 NAME"),16-1);
    strncpy(filt2,cpl_propertylist_get_string(plist,"ESO INS FILT2 NAME"),16-1);
    filt1[16-1] = '\0';
    filt2[16-1] = '\0';

    if (cpl_errorstate_is_equal(prestate)) {
        if (! strncmp(filt1,"OPEN",16)) {
            strncpy(filt,filt2,16);
        } else {
            strncpy(filt,filt1,16);
        }
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of NDIT
    \param    plist       property list to read from
    \param    ndit        requested value of ndit
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_ndit(const cpl_propertylist *plist, int *ndit) {
    int val;

    val = cpl_propertylist_get_int(plist,"ESO DET NDIT");
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        *ndit = val;
        return(CASU_OK);
    } else {
        cpl_error_reset();
        *ndit = 1;
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of NDSAMPLES
    \param    plist       property list to read from
    \param    ndsamp      requested value of ndit
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_ndsamp(const cpl_propertylist *plist, int *ndsamp) {
    int val;

    val = cpl_propertylist_get_int(plist,"ESO DET NDSAMPLES");
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        *ndsamp = val;
        return(CASU_OK);
    } else {
        cpl_error_reset();
        *ndsamp = 1;
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of exposure time
    \param    plist       property list to read from
    \param    exptime     requested value of exptime
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_exptime(const cpl_propertylist *plist, 
                                   float *exptime) {

    return(hawki_pfits_get_float(plist,"EXPTIME",exptime));

}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of DET_LIVE
    \param    plist       property list to read from
    \param    detlive     requested value of DET_LIVE
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_detlive(const cpl_propertylist *plist, 
                                   int *detlive) {
    int val;

    val = cpl_propertylist_get_bool(plist,"ESO DET CHIP LIVE");
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        *detlive = val;
        return(CASU_OK);
    } else {
        cpl_error_reset();
        *detlive = 1;
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of the modified Julian date
    \param    plist       property list to read from
    \param    mjd        requested value of the mjd
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_mjd(const cpl_propertylist *plist, double *mjd) {

    return(hawki_pfits_get_double(plist,"MJD-OBS",mjd));

}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of the template start time
    \param    plist       property list to read from
    \param    tplstart    the template start time
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_tplstart(cpl_propertylist *plist, char *tplstart) {
    strcpy(tplstart,cpl_propertylist_get_string(plist,"ESO TPL START"));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get the value of DATE-OBS
    \param    plist       property list to read from
    \param    dateobs     the template start time
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

extern int hawki_pfits_get_dateobs(cpl_propertylist *plist, char *dateobs) {
    strcpy(dateobs,cpl_propertylist_get_string(plist,"DATE-OBS"));
    if (cpl_error_get_code() == CPL_ERROR_NONE) {
        return(CASU_OK);
    } else {
        cpl_error_reset();
        return(CASU_FATAL);
    }
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get a floating point value
    \param    plist       property list to read from
    \param    key         the keyword for the property
    \param    fval        the output floating point value
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

static int hawki_pfits_get_float(const cpl_propertylist *plist, 
                                 const char *key, float *fval) {
    cpl_type type;
    const char *fctid = "hawki_pfits_get_float";

    /* Get the type of this keyword */

    type = cpl_propertylist_get_type(plist,key);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        *fval = 0.0;
        cpl_error_reset();
        return(CASU_FATAL);
    }

    /* Switch of the property type */

    switch (type) {
    case CPL_TYPE_FLOAT:
        *fval = cpl_propertylist_get_float(plist,key);
        break;
    case CPL_TYPE_DOUBLE:
        *fval = (float)cpl_propertylist_get_double(plist,key);
        break;
    default:
        *fval = 0.0;
        cpl_msg_error(fctid,"Keyword %s is not floating point in header",key);
        return(CASU_FATAL);
    }
    return(CASU_OK);
}

/*---------------------------------------------------------------------------*/
/**
    \brief    Get a double precision value
    \param    plist       property list to read from
    \param    key         the keyword for the property
    \param    fval        the output double precision value
    \retval CASU_OK if everything went right 
    \retval CASU_FATAL if there was an error
 */
/*---------------------------------------------------------------------------*/

static int hawki_pfits_get_double(const cpl_propertylist *plist, 
                                  const char *key, double *fval) {
    cpl_type type;
    const char *fctid = "hawki_pfits_get_float";

    /* Get the type of this keyword */

    type = cpl_propertylist_get_type(plist,key);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        *fval = 0.0;
        cpl_error_reset();
        return(CASU_FATAL);
    }

    /* Switch of the property type */

    switch (type) {
    case CPL_TYPE_FLOAT:
        *fval = (double)cpl_propertylist_get_float(plist,key);
        break;
    case CPL_TYPE_DOUBLE:
        *fval = cpl_propertylist_get_double(plist,key);
        break;
    default:
        *fval = 0.0;
        cpl_msg_error(fctid,"Keyword %s is not floating point in header",key);
        return(CASU_FATAL);
    }
    return(CASU_OK);
}


/**@}*/

/*

$Log: hawki_pfits.c,v $
Revision 1.6  2015/08/07 13:07:06  jim
Fixed copyright to ESO

Revision 1.5  2015/01/29 12:00:28  jim
modified comments

Revision 1.4  2014/12/11 12:24:35  jim
Lots of upgrades

Revision 1.3  2013/11/21 09:38:14  jim
detabbed

Revision 1.2  2013-10-24 09:28:44  jim
Added hawki_pfits_get_dateobs

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/

