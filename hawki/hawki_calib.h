/* $Id: hawki_calib.h,v 1.7 2013-03-25 11:35:06 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:06 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_CALIB_H
#define HAWKI_CALIB_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Function definitions
 -----------------------------------------------------------------------------*/

int hawki_bpm_calib
(cpl_image *           ima,
 const char *          bpm,
 int                   idet);
int hawki_flat_bpm_detector_calib
(cpl_imagelist       *   ilist,
 cpl_image           *   flat,
 cpl_image           *   bpm);
int hawki_flat_dark_bpm_detector_calib
(cpl_imagelist       *   ilist,
 cpl_image           *   flat,
 cpl_image           *   dark,
 cpl_image           *   bpm);
cpl_imagelist *  hawki_trim_detector_calib
(cpl_imagelist       *   ilist,
 int                     nborder);
int hawki_flat_dark_bpm_imglist_calib
(cpl_imagelist *       ilist,
 cpl_imagelist *       flat,
 cpl_imagelist *       dark,
 cpl_imagelist *       bpm);
int hawki_flat_bpm_imglist_calib
(cpl_imagelist *       ilist,
 cpl_imagelist *       flat,
 cpl_imagelist *       bpm);
int hawki_bkg_imglist_calib
(cpl_imagelist *       ilist,
 cpl_imagelist *       bkg);
#endif
