/* $Id: hawki_load.h,v 1.23 2013-03-25 11:35:08 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:08 $
 * $Revision: 1.23 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_LOAD_H
#define HAWKI_LOAD_H

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

CPL_BEGIN_DECLS
cpl_frameset  * hawki_extract_frameset(const cpl_frameset *, const char *) ;
cpl_imagelist * hawki_load_frameset(const cpl_frameset *, int, cpl_type) ;
cpl_imagelist * hawki_load_frame(const cpl_frame *, cpl_type) ;
cpl_image     * hawki_load_frame_detector(const cpl_frame *, int, cpl_type) ;
cpl_image     * hawki_load_frame_extension(const cpl_frame *, int, cpl_type) ;
cpl_image     * hawki_load_image(const cpl_frameset *, int, int, cpl_type) ;
cpl_imagelist * hawki_load_detector(const cpl_frameset *, int, cpl_type) ;
cpl_imagelist * hawki_load_extensions(const cpl_frameset *, int, cpl_type) ;
cpl_imagelist * hawki_load_quadrants(const cpl_frameset *, int, int, cpl_type) ;
cpl_image     * hawki_load_quadrant(const cpl_frameset *, int, int, int, cpl_type) ;
cpl_image     * hawki_load_quadrant_from_file(const char *, int, int, cpl_type) ;
cpl_table    ** hawki_load_tables(const cpl_frame * frame);
cpl_bivector ** hawki_load_refined_offsets(const cpl_frame * offsets_frame);
cpl_image     * hawki_rebuild_quadrants(const cpl_image *, const cpl_image *, 
                                        const cpl_image *, const cpl_image *) ;
cpl_image     * hawki_rebuild_detectors(const cpl_image *, const cpl_image *, 
                                        const cpl_image *, const cpl_image *) ;
int hawki_get_detector_from_ext(const char *, int) ;
int hawki_get_ext_from_detector(const char *, int) ;
int * hawki_get_ext_detector_mapping
(const char  *  fname);
CPL_END_DECLS

#endif
