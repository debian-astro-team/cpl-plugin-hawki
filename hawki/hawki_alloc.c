/* $Id: hawki_alloc.c,v 1.2 2013-03-25 11:35:04 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:04 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include "hawki_utils_legacy.h"
#include "hawki_alloc.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_alloc       Allocation rutines
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/** 
  @brief  Deallocates a set of four tables  
  @param  tables  The tables to deallocate
  @return   0 if everything is ok, -1 otherwise

 */ 
int hawki_table_delete(cpl_table ** table)
{
    int idet;
    
    for(idet = 0; idet < HAWKI_NB_DETECTORS; ++idet)
        cpl_table_delete(table[idet]);
    cpl_free(table);
    if(cpl_errorstate_get() != CPL_ERROR_NONE)
        return -1;
    return 0;
}

/** 
  @brief  Allocates a set of four tables  
  @param  nrow  The number of rows of each table
  @return   The allocated tables

 */ 
cpl_table ** hawki_table_new(int nrow)
{
    int idet;
    cpl_table ** tables;

    cpl_errorstate prestate = cpl_errorstate_get();

    tables = cpl_malloc(sizeof(cpl_table *) * HAWKI_NB_DETECTORS);
    for(idet = 0; idet < HAWKI_NB_DETECTORS; ++idet) {
        tables[idet] = cpl_table_new(nrow);
    }

    if (!cpl_errorstate_is_equal(prestate)) {
        for(idet = 0; idet < HAWKI_NB_DETECTORS; ++idet) {
            if (tables != NULL){
                cpl_table_delete(tables[idet]);
            }
            cpl_free(tables);
        }
        return NULL;
    } else {
        return tables;
    }

}

/**@}*/
