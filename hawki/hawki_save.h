/* $Id: hawki_save.h,v 1.6 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_SAVE_H
#define HAWKI_SAVE_H

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include "hawki_distortion.h"

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

CPL_BEGIN_DECLS
int hawki_imagelist_save
(cpl_frameset                *   allframes,
 const cpl_parameterlist     *   parlist,
 const cpl_frameset          *   usedframes,
 const cpl_imagelist         *   images,
 const char                  *   recipe,
 const char                  *   procat,
 const char                  *   protype,
 const cpl_propertylist      *   applist,
 const cpl_propertylist      **  applists,
 const char                  *   filename);
int hawki_images_save
(cpl_frameset                *   allframes,
 const cpl_parameterlist     *   parlist,
 const cpl_frameset          *   usedframes,
 const cpl_image             **  images,
 const char                  *   recipe,
 const char                  *   procat,
 const char                  *   protype,
 const cpl_propertylist      *   applist,
 const cpl_propertylist      **  applists,
 const char                  *   filename);
int hawki_tables_save
(cpl_frameset                *   allframes,
 const cpl_parameterlist     *   parlist,
 const cpl_frameset          *   usedframes,
 const cpl_table             **  tables,
 const char                  *   recipe,
 const char                  *   procat,
 const char                  *   protype,
 const cpl_propertylist      *   applist,
 const cpl_propertylist      **  applists,
 const char                  *   filename);
int hawki_distortion_save
(cpl_frameset                *   allframes,
 const cpl_parameterlist     *   parlist,
 const cpl_frameset          *   usedframes,
 const hawki_distortion      **  distortion,
 const char                  *   recipe,
 const cpl_propertylist      *   applist,
 const cpl_propertylist      **  applists,
 const char                  *   filename_x,
 const char                  *   filename_y);
int hawki_main_header_save
(cpl_frameset                *   allframes,
 const cpl_parameterlist     *   parlist,
 const cpl_frameset          *   usedframes,
 const char                  *   recipe,
 const char                  *   procat,
 const char                  *   protype,
 const cpl_propertylist      *   applist,
 const char                  *   filename);
int hawki_image_ext_save
(const cpl_frameset      *   allframes,
 const cpl_image         *   image,
 int                         iext,
 const cpl_propertylist  *   ext_prop_list,
 const char              *   filename);
const char * hawki_get_extref_file(const cpl_frameset *);
CPL_END_DECLS

#endif
