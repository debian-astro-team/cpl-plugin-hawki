/* $Id: hawki_utils-test.c,v 1.1 2015/10/16 09:48:56 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/16 09:48:56 $
 * $Revision: 1.1 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_mods.h>
#include <casu_mask.h>
#include <hawki_var.h>
#include <hawki_utils.h>

int main(void) {
    cpl_propertylist *p1,*p2;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check hawki_updatewcs */

    p1 = cpl_propertylist_new();
    hawki_updatewcs(p1);
    cpl_test_eq_string(CTYPE1,cpl_propertylist_get_string(p1,"CTYPE1"));
    cpl_test_eq_string(CTYPE2,cpl_propertylist_get_string(p1,"CTYPE2"));
    cpl_test_rel(PV2_1,cpl_propertylist_get_float(p1,"PV2_1"),0.01);
    cpl_test_rel(PV2_3,cpl_propertylist_get_float(p1,"PV2_3"),0.01);
    cpl_test_rel(PV2_5,cpl_propertylist_get_float(p1,"PV2_5"),0.01);

    /* Check hawki_copywcs */

    cpl_propertylist_update_double(p1,"CRVAL1",30.0);
    cpl_propertylist_update_double(p1,"CRVAL2",-12.0);
    cpl_propertylist_update_double(p1,"CRPIX1",1024.0);
    cpl_propertylist_update_double(p1,"CRPIX2",1024.0);
    cpl_propertylist_update_double(p1,"CD1_1",0.5);
    cpl_propertylist_update_double(p1,"CD1_2",-0.5);
    cpl_propertylist_update_double(p1,"CD2_1",-0.5);
    cpl_propertylist_update_double(p1,"CD2_2",0.5);
    p2 = cpl_propertylist_new();
    hawki_copywcs(p1,p2);
    cpl_test_eq_string(CTYPE1,cpl_propertylist_get_string(p2,"CTYPE1"));
    cpl_test_eq_string(CTYPE2,cpl_propertylist_get_string(p2,"CTYPE2"));
    cpl_test_rel(PV2_1,cpl_propertylist_get_float(p2,"PV2_1"),0.01);
    cpl_test_rel(PV2_3,cpl_propertylist_get_float(p2,"PV2_3"),0.01);
    cpl_test_rel(PV2_5,cpl_propertylist_get_float(p2,"PV2_5"),0.01);
    cpl_test_rel(30.0,cpl_propertylist_get_double(p2,"CRVAL1"),0.01);
    cpl_test_rel(-12.0,cpl_propertylist_get_double(p2,"CRVAL2"),0.01);
    cpl_test_rel(1024.0,cpl_propertylist_get_double(p2,"CRPIX1"),0.01);
    cpl_test_rel(1024.0,cpl_propertylist_get_double(p2,"CRPIX2"),0.01);
    cpl_test_rel(0.5,cpl_propertylist_get_double(p2,"CD1_1"),0.01);
    cpl_test_rel(-0.5,cpl_propertylist_get_double(p2,"CD1_2"),0.01);
    cpl_test_rel(-0.5,cpl_propertylist_get_double(p2,"CD2_1"),0.01);
    cpl_test_rel(0.5,cpl_propertylist_get_double(p2,"CD2_2"),0.01);
    
    /* Get out of here */

    cpl_propertylist_delete(p1);
    cpl_propertylist_delete(p2);
    return(cpl_test_end(0));

}

/*

$Log: hawki_utils-test.c,v $
Revision 1.1  2015/10/16 09:48:56  jim
new


*/
