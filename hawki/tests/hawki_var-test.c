/* $Id: hawki_var-test.c,v 1.1 2015/10/16 09:48:56 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/16 09:48:56 $
 * $Revision: 1.1 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <casu_mask.h>
#include <hawki_var.h>

int main(void) {
    cpl_image *im1,*im2;
    casu_fits *f1,*f2,*v1,*v2;
    casu_mask *mask;
    unsigned char *bpm;
    cpl_propertylist *phu,*ehu;
    float readnoise=10.0,gain=2.0,val1,val2;
    int junk;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Create images */

    im1 = cpl_image_new(10,10,CPL_TYPE_FLOAT);
    cpl_image_add_scalar(im1,1000.0);
    phu = cpl_propertylist_new();
    cpl_propertylist_update_int(phu,"ESO DET NDIT",2);
    cpl_propertylist_update_int(phu,"ESO DET NSAMPLES",10);
    ehu = cpl_propertylist_new();
    f1 = casu_fits_wrap(im1,NULL,phu,ehu);
    cpl_propertylist_delete(phu);
    cpl_propertylist_delete(ehu);
    im2 = cpl_image_new(10,10,CPL_TYPE_FLOAT);
    cpl_image_add_scalar(im2,2000.0);
    phu = cpl_propertylist_new();
    cpl_propertylist_update_int(phu,"ESO DET NDIT",2);
    cpl_propertylist_update_int(phu,"ESO DET NSAMPLES",10);
    ehu = cpl_propertylist_new();
    f2 = casu_fits_wrap(im2,NULL,phu,ehu);
    cpl_propertylist_delete(phu);
    cpl_propertylist_delete(ehu);
    bpm = cpl_calloc(100,sizeof(unsigned char));
    mask = casu_mask_wrap_bpm(bpm,10,10);

    /* Create the two variance arrays */

    
    v1 = hawki_var_create(f1,mask,readnoise,gain);
    v2 = hawki_var_create(f2,mask,readnoise,gain);
    val1 = (float)cpl_image_get(casu_fits_get_image(v1),5,5,&junk);
    val2 = (float)cpl_image_get(casu_fits_get_image(v2),5,5,&junk);
    cpl_test_rel(350,val1,0.01);
    cpl_test_rel(600,val2,0.01);

    /* Now add them */

    hawki_var_add(v1,v2);
    val1 = (float)cpl_image_get(casu_fits_get_image(v1),5,5,&junk);
    val2 = (float)cpl_image_get(casu_fits_get_image(v2),5,5,&junk);
    cpl_test_rel(950,val1,0.01);
    cpl_test_rel(600,val2,0.01);

    /* Now divide them */

    hawki_var_div_im(v1,v2);
    val1 = (float)cpl_image_get(casu_fits_get_image(v1),5,5,&junk);
    val2 = (float)cpl_image_get(casu_fits_get_image(v2),5,5,&junk);
    cpl_test_rel(0.00264,val1,0.01);
    cpl_test_rel(600,val2,0.01);

    /* Now divide the second one by a constant */

    hawki_var_divk(v2,2.0);
    val1 = (float)cpl_image_get(casu_fits_get_image(v1),5,5,&junk);
    val2 = (float)cpl_image_get(casu_fits_get_image(v2),5,5,&junk);
    cpl_test_rel(0.00264,val1,0.01);
    cpl_test_rel(150,val2,0.01);

    /* Get out of here */

    casu_fits_delete(v1);
    casu_fits_delete(v2);
    casu_fits_delete(f1);
    casu_fits_delete(f2);
    casu_mask_delete(mask);
    return(cpl_test_end(0));

}

/*

$Log: hawki_var-test.c,v $
Revision 1.1  2015/10/16 09:48:56  jim
new


*/
