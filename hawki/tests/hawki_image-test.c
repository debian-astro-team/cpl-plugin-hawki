/* $Id: hawki_image-test.c,v 1.5 2013-08-07 13:50:03 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-08-07 13:50:03 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include "cpl_test.h"
#include "hawki_image.h"

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/
#define cpl_drand() ((double)rand()/(double)RAND_MAX)

/*-----------------------------------------------------------------------------
                                   Static functions
 -----------------------------------------------------------------------------*/
static void hawki_image_all_test(void);
static void hawki_image_copy_to_intersection_test(void);

/*-----------------------------------------------------------------------------
                                  Main
 -----------------------------------------------------------------------------*/
int main (void)
{

    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    hawki_image_all_test();

    return cpl_test_end(0);
}

static void hawki_image_all_test(void)
{
    hawki_image_copy_to_intersection_test();
}

static void hawki_image_copy_to_intersection_test(void)
{
    /* Case 1: image from is contained within image target. 
     * Check intersection values are the same */
    {
        cpl_size from_nx = 100;
        cpl_size from_ny = 100;
        cpl_size target_nx = 300;
        cpl_size target_ny = 300;
        cpl_size offset_x = -100; 
        cpl_size offset_y = -100; 
        
        cpl_image * from;
        cpl_image * target;
        
        FILE * dumpfrom;
        FILE * dumptarget;
        
        
        double val_target;
        double val_from;
        int    pix_reject;
        
        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);        
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);

        cpl_test_error(CPL_ERROR_NONE);

        /* Dump the intersection of target */
        dumptarget = fopen("target_dump", "w");
        cpl_test_nonnull(dumptarget);
        cpl_image_dump_window(target, 101, 101, 200, 200, dumptarget);
        cpl_test_error(CPL_ERROR_NONE);

        /* Dump the intersection of from */
        dumpfrom = fopen("from_dump", "w");
        cpl_test_nonnull(dumpfrom);
        cpl_image_dump_window(from, 1, 1, 100, 100, dumpfrom);
        cpl_test_error(CPL_ERROR_NONE);

        fclose(dumptarget);
        fclose(dumpfrom);

        /* Compare a couple of values */
        val_target = cpl_image_get(target, 101, 101, &pix_reject);
        val_from = cpl_image_get(from, 1, 1, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 200, 200, &pix_reject);
        val_from = cpl_image_get(from, 100, 100, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);

       /* Delete files */
       cpl_test_zero( remove("target_dump") );
       cpl_test_zero( remove("from_dump") );
    }
     
    /* Case 2: image from contains image target */
    {
        cpl_size from_nx = 300;
        cpl_size from_ny = 300;
        cpl_size target_nx = 100;
        cpl_size target_ny = 100;
        cpl_size offset_x = 100; 
        cpl_size offset_y = 100; 

        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pix_reject;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny, CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);

        /* Compare a couple of values */
        val_target = cpl_image_get(target, 1, 1, &pix_reject);
        val_from = cpl_image_get(from, 101, 101, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 100, 1, &pix_reject);
        val_from = cpl_image_get(from, 200, 101, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 100, 100, &pix_reject);
        val_from = cpl_image_get(from, 200, 200, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 3: image from and target overlaps in the left bottom corner */
    {
        cpl_size from_nx = 100;
        cpl_size from_ny = 100;
        cpl_size target_nx = 100;
        cpl_size target_ny = 100;
        cpl_size offset_x = -50; 
        cpl_size offset_y = -50; 

        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pix_reject;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny, CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);

        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);

        /* Compare a couple of values */
        val_target = cpl_image_get(target, 51, 51, &pix_reject);
        val_from = cpl_image_get(from, 1, 1, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 51, 52, &pix_reject);
        val_from = cpl_image_get(from, 1, 2, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 4: image from and target overlaps in the right upper corner */
    {
        cpl_size from_nx = 100;
        cpl_size from_ny = 100;
        cpl_size target_nx = 100;
        cpl_size target_ny = 100;
        cpl_size offset_x = (cpl_size)(cpl_drand() * 50 +1); 
        cpl_size offset_y = (cpl_size)(cpl_drand() * 50 +1); 

        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pix_reject;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Compare a couple of values */
        val_target = cpl_image_get(target, 1, 1, &pix_reject);
        val_from = cpl_image_get(from, 1 + offset_x, 1 + offset_y, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 100 - offset_x, 100 - offset_y, &pix_reject);
        val_from = cpl_image_get(from, 100, 100, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 5: images are the same */
    {
        cpl_size from_nx = 100;
        cpl_size from_ny = 100;
        cpl_size target_nx = 100;
        cpl_size target_ny = 100;
        cpl_size offset_x = 0; 
        cpl_size offset_y = 0; 

        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pix_reject;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Compare a couple of values */
        val_target = cpl_image_get(target, 1, 1, &pix_reject);
        val_from = cpl_image_get(from, 1, 1, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 100, 100, &pix_reject);
        val_from = cpl_image_get(from, 100, 100, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 6: image from overruns only in the left side the target */
    {
        cpl_size from_nx = 100;
        cpl_size from_ny = 50;
        cpl_size target_nx = 100;
        cpl_size target_ny = 100;
        cpl_size offset_x = 25; 
        cpl_size offset_y = -25; 

        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pix_reject;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);

        /* Compare a couple of values */
        val_target = cpl_image_get(target, 1, 26, &pix_reject);
        val_from = cpl_image_get(from, 26, 1, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 75, 75, &pix_reject);
        val_from = cpl_image_get(from, 100, 50, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 7: image from overruns only in the upper side the target */
    {
        cpl_size from_nx = 50;
        cpl_size from_ny = 100;
        cpl_size target_nx = 100;
        cpl_size target_ny = 100;
        cpl_size offset_x = -25; 
        cpl_size offset_y = -25;

        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pix_reject;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Compare a couple of values */
        val_target = cpl_image_get(target, 26, 26, &pix_reject);
        val_from = cpl_image_get(from, 1, 1, &pix_reject);
        cpl_test_eq( val_target, val_from );
        val_target = cpl_image_get(target, 75, 100, &pix_reject);
        val_from = cpl_image_get(from, 50, 75, &pix_reject);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 8: very small images */
    {
        cpl_size from_nx = 2;
        cpl_size from_ny = 2;
        cpl_size target_nx = 2;
        cpl_size target_ny = 2;
        cpl_size offset_x = 1; 
        cpl_size offset_y = 1;
        
        cpl_image * from;
        cpl_image * target;
        
        double val_target;
        double val_from;    
        int     pixel_rejected;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_DOUBLE);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_DOUBLE);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);

        val_target = cpl_image_get(target, 1, 1, &pixel_rejected);
        val_from = cpl_image_get(from, 2, 2, &pixel_rejected);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }
    /* Case 9: very small images. Integer case */
    {
        cpl_size from_nx = 2;
        cpl_size from_ny = 2;
        cpl_size target_nx = 2;
        cpl_size target_ny = 2;
        cpl_size offset_x = 1; 
        cpl_size offset_y = 1;
        
        cpl_image * from;
        cpl_image * target;

        double val_target;
        double val_from;
        int    pixel_rejected;

        /* Create images */
        from = cpl_image_new(from_nx, from_ny, CPL_TYPE_INT);
        target = cpl_image_new(target_nx, target_ny,CPL_TYPE_INT);
        cpl_test_nonnull(from);
        cpl_test_nonnull(target);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_test_error(CPL_ERROR_NONE);
        
        /* Call the tested function */
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);

        val_target = cpl_image_get(target, 1, 1, &pixel_rejected);
        val_from = cpl_image_get(from, 2, 2, &pixel_rejected);
        cpl_test_eq( val_target, val_from );

        /* Free memory */
        cpl_image_delete(from);
        cpl_image_delete(target);
    }

    /* Case 10: null from image */
    {
        cpl_image *from = NULL;
        cpl_image *target = cpl_image_new(1, 1, CPL_TYPE_INT);
        cpl_size offset_x = 1;
        cpl_size offset_y = 1;
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NULL_INPUT);
        cpl_image_delete(target);
    }

    /* Case 11: null target image */
    {
        cpl_image *from = cpl_image_new(1, 1, CPL_TYPE_INT);
        cpl_image *target = NULL;
        cpl_size offset_x = 1;
        cpl_size offset_y = 1;
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NULL_INPUT);
        cpl_image_delete(from);
    }

    /* Case 12: both inputs NULL */
    {
        cpl_image *from = NULL;
        cpl_image *target = NULL;
        cpl_size offset_x = 1;
        cpl_size offset_y = 1;
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NULL_INPUT);
    }

    /* Case 13: images not the same type */
    {
        cpl_image *from = cpl_image_new(1, 1, CPL_TYPE_INT);
        cpl_image *target = cpl_image_new(1, 1, CPL_TYPE_DOUBLE);
        cpl_size offset_x = 1;
        cpl_size offset_y = 1;
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_TYPE_MISMATCH);
        cpl_image_delete(from);
        cpl_image_delete(target);
    }

    /* Case 14: images that do not overlap */
    {
        cpl_image *from = cpl_image_new(10, 10, CPL_TYPE_DOUBLE);
        cpl_image *target = cpl_image_new(10, 10, CPL_TYPE_DOUBLE);
        cpl_image_fill_noise_uniform(from, 1., 10.);
        cpl_image_fill_noise_uniform(target, 1., 10.);
        cpl_image *target_orig = cpl_image_duplicate(target);
        cpl_size offset_x = 11;
        cpl_size offset_y = 11;
        hawki_image_copy_to_intersection(target, from, offset_x, offset_y);
        cpl_test_error(CPL_ERROR_NONE);
        cpl_test_image_abs(target, target_orig, 0.);
        cpl_image_delete(from);
        cpl_image_delete(target);
        cpl_image_delete(target_orig);
    }

}
