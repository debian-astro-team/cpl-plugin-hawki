/* $Id: hawki_pfits.c,v 1.20 2013-03-25 11:35:10 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:10 $
 * $Revision: 1.20 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <string.h>
#include <cpl.h>

#include "hawki_pfits_legacy.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup hawki_pfits     FITS header protected access
 *
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
  							Function codes
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    find out airmass start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_airmass_start(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AIRM START") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out airmass end
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_airmass_end(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AIRM END") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the humidity level
  @param    plist       property list to read from
  @return   the requested value 
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_humidity_level(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI RHUM") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the PRO.CATG
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * hawki_pfits_get_procatg(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "ESO PRO CATG");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the target RA
  @param    plist       property list to read from
  @return   the requested value in degrees
  
  This function decodifies the returned value, since it is in HHMMSS.SS
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_targ_alpha(const cpl_propertylist * plist)
{
    double alpha_coded;
    double ra_hh, ra_mm, ra_ss;
    double ra_deg;
    alpha_coded = cpl_propertylist_get_double(plist, "ESO TEL TARG ALPHA");
    ra_hh = (int)(alpha_coded / 10000);
    ra_mm = (int)((alpha_coded - ra_hh * 10000) / 100);
    ra_ss = alpha_coded - ra_hh * 10000 - ra_mm * 100;
    ra_deg = ra_hh * 15. + ra_mm / 4. + ra_ss / 240.;
    return ra_deg;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the target RA in HHMMSS format
  @param    plist       property list to read from
  @return   the requested value in degrees
  
  This function reads the keywords as it is, in HHMMSS.SS format
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_targ_alpha_hhmmss(const cpl_propertylist * plist)
{
    double alpha_coded;
    alpha_coded = cpl_propertylist_get_double(plist, "ESO TEL TARG ALPHA");
    return alpha_coded;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the target DEC
  @param    plist       property list to read from
  @return   the requested value in degrees
  
  This function decodifies the returned value, since it is in DDMMSS.SS
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_targ_delta(const cpl_propertylist * plist)
{
    double delta_coded;
    double dec_dd, dec_mm, dec_ss;
    double dec_deg;
    delta_coded = cpl_propertylist_get_double(plist, "ESO TEL TARG DELTA");
    dec_dd = (int)(delta_coded / 10000);
    dec_mm = (int)((delta_coded - dec_dd * 10000) / 100);
    dec_ss = delta_coded - dec_dd * 10000 - dec_mm * 100;
    dec_deg = dec_dd + dec_mm / 60. + dec_ss / 3600.;
    return dec_deg;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the target DEC in DDMMSS format
  @param    plist       property list to read from
  @return   the requested value in degrees
  
  This function reads the keywords as it is, in DDMMSS.SS format
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_targ_delta_ddmmss(const cpl_propertylist * plist)
{
    double delta_coded;
    delta_coded = cpl_propertylist_get_double(plist, "ESO TEL TARG DELTA");
    return delta_coded;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the target equinox
  @param    plist       property list to read from
  @return   the requested value in years
  
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_targ_equinox(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL TARG EQUINOX") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the target epoch
  @param    plist       property list to read from
  @return   the requested value in years
  
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_targ_epoch(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL TARG EPOCH") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the RA
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_ra(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "RA") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the DEC
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_dec(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "DEC") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the extname
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * hawki_pfits_get_extname(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "EXTNAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the arcfile   
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * hawki_pfits_get_arcfile(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "ARCFILE");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the date of observation 
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * hawki_pfits_get_date_obs(const cpl_propertylist * plist)
{
    return (const char *) cpl_propertylist_get_string(plist, "DATE-OBS") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NEXP value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int hawki_pfits_get_nexp(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO TPL NEXP")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the DIT value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_dit_legacy(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO DET DIT")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NDIT value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int hawki_pfits_get_ndit_legacy(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO DET NDIT")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NDSAMPLES value 
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int hawki_pfits_get_ndsamples(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "ESO DET NDSAMPLES")  ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the pixel scale
  @param    plist       property list to read from
  @return   the requested value 
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_pixscale(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO INS PIXSCALE") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the number of pixels in axis 1
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int hawki_pfits_get_naxis1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "NAXIS1") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the number of pixels in axis 2
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int hawki_pfits_get_naxis2(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_int(plist, "NAXIS2") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the value in WCS coordinates of the reference pixel (axe 1)
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_crval1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRVAL1") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the value in WCS coordinates of the reference pixel (axe 2)
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_crval2(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRVAL2") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the reference pixel in axe 1
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_crpix1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRPIX1") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the reference pixel in axe 1
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_crpix2(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CRPIX2") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the increment of coordinate in one pixel (axe 1)
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_cdelta1(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CDELTA1") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the increment of coordinate in one pixel (axe 2)
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_cdelta2(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "CDELTA2") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative offset in alpha
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_cumoffseta(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO SEQ CUMOFFSETA") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative offset in delta
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_cumoffsetd(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO SEQ CUMOFFSETD") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative offset in X
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_cumoffsetx(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO SEQ CUMOFFSETX") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative offset in Y
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_cumoffsety(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO SEQ CUMOFFSETY") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the position angle
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_posangle(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO ADA POSANG") ;
}

/*----------------------------------------------------------------------------*/
/** 
  @brief    find out which wave band is active
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * hawki_pfits_get_filter_legacy(const cpl_propertylist * plist)
{
    const char  *   val ;

    val = cpl_propertylist_get_string(plist, "ESO INS FILT1 NAME") ;
    if (val == NULL) return NULL ;
    /* If FILT1 is not open, return its value */
    if (strcmp(val, "open") && strcmp(val, "OPEN")) return val ;
    /* FILT1 is open, return value from FILT2 */
    val = cpl_propertylist_get_string(plist, "ESO INS FILT2 NAME") ;
    if (val == NULL) return NULL ;
    if (strcmp(val, "open") && strcmp(val, "OPEN")) return val ;
    return NULL ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the focus position of M2
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_focus(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL FOCU VALUE") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the telescope altitude
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_elevation(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL ALT") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the telescope azimut
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_azimut(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AZ") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the relative humidity
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_relhum(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI RHUM") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the average coherence time
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_tau0(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI TAU0") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the ambient temperatur
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_obs_temp(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI TEMP") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the wind direction
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_wind_dir(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI WINDDIR") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the wind speed
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_wind_speed(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI WINDSP") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the fwhm from the AO system
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_ao_fwhm(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL IA FWHM") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the absolute rotator position at start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_rotator_start(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO ADA ABSROT START") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the absolute rotator position at end
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_rotator_end(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO ADA ABSROT END") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the observatory seeing at start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_obs_seeing_start(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI FWHM START") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the observatory seeing at end
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_obs_seeing_end(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI FWHM END") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the observatory atmospheric pressure at start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_pressure_start(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI PRES START") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the observatory atmospheric pressure at end
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_pressure_end(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL AMBI PRES END") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the paralactic angle at start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_parangle_start(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL PARANG START") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the paralactic angle at start
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_parangle_end(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO TEL PARANG END") ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative X offset of the combined image
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/

double hawki_pfits_get_comb_cumoffsetx(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC COMBINED CUMOFFSETX");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative Y offset of the combined image
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_comb_cumoffsety(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC COMBINED CUMOFFSETY");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the relative X offset of the combined image wrt first image
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_comb_posx(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC COMBINED POSX");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the relative Y offset of the combined image wrt first image
  @param    plist       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double hawki_pfits_get_comb_posy(const cpl_propertylist * plist)
{
    return cpl_propertylist_get_double(plist, "ESO QC COMBINED POSY");
}

/**@}*/
