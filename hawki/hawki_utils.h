/* $Id: hawki_utils.h,v 1.7 2015/11/27 12:22:51 jim Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/11/27 12:22:51 $
 * $Revision: 1.7 $
 * $Name:  $
 */


/* Includes */

#ifndef HAWKI_UTILS_H
#define HAWKI_UTILS_H

#include <casu_fits.h>
#include <casu_tfits.h>

#define SZ_ALLDESC 4096
#define HAWKI_NEXTN 4

#define HAWKI_CHAN_WIDTH 128
#define HAWKI_NX         2048
#define HAWKI_NY         2048

/* Default WCS projection */

#define CTYPE1 "RA---ZPN"
#define CTYPE2 "DEC--ZPN"
#define PV2_1  1.0
#define PV2_3  400.0
#define PV2_5  0.0

/* Function prototypes */
int hawki_aodata_nextn_correct(int nextn);

cpl_error_code hawki_propagate_aoextensions(cpl_frameset * frameset,
                                            const char * from, const char * to);

extern const char *hawki_get_license(void);
extern cpl_table *hawki_create_diffimg_stats(int nrows);
extern void hawki_difference_image(cpl_image *master, cpl_image *prog, 
                                   unsigned char *bpm, int ncells, int oper, 
                                   float *global_diff, float *global_rms, 
                                   cpl_image **diffim, 
                                   cpl_table **diffimstats);
extern void hawki_wcsfit_multi(casu_fits **in, casu_tfits **incat, 
                               char *catname, char *catpath, int cdssearch,
                               char *cacheloc, int keepms, casu_tfits **outms);
extern int hawki_testfrms(cpl_frameset *frms, int nextn_expected, int isimg, 
                          int checkwcs);
extern int hawki_testfrm_1(cpl_frame *fr, int nextn_expected, int isimg,
                           int checkwcs);
extern int hawki_testrdgn(cpl_frame *fr, cpl_frame *readgain);
extern void hawki_getrdgn(cpl_frame *readgain, char *extname, float *readnoise,
                          float *gain);
extern void hawki_updatewcs(cpl_propertylist *p);
extern void hawki_copywcs(cpl_propertylist *p1, cpl_propertylist *p2);

#endif

/* 

$Log: hawki_utils.h,v $
Revision 1.7  2015/11/27 12:22:51  jim
Added cacheloc parameter

Revision 1.6  2015/09/15 10:29:13  jim
added hawki_copywcs

Revision 1.5  2015/08/07 13:07:06  jim
Fixed copyright to ESO

Revision 1.4  2015/02/14 12:36:05  jim
Expanded and modified hawki_wcsfit_multi

Revision 1.3  2015/01/29 11:59:01  jim
Added support routines that used to be in with the recipe source code

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/

