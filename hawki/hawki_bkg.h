/* $Id: hawki_bkg.h,v 1.12 2013-03-25 11:35:05 cgarcia Exp $
 *
 * This file is part of the HAWKI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * $Author: cgarcia $
 * $Date: 2013-03-25 11:35:05 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifndef HAWKI_BKG_H
#define HAWKI_BKG_H

/*-----------------------------------------------------------------------------
   								Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

typedef struct _hawki_bkg_frames_buffer_ hawki_bkg_frames_buffer;

struct _hawki_bkg_frames_buffer_
{
    cpl_image   ** images;
    double      * medians;
    cpl_frameset * frames;
    cpl_size       nframes;
};

CPL_BEGIN_DECLS

hawki_bkg_frames_buffer * hawki_bkg_frames_buffer_init
(const cpl_frameset * fset);

void hawki_bkg_frames_buffer_delete(hawki_bkg_frames_buffer * frames_buffer);

int hawki_bkg_fill_assoc(cpl_frameset * objframes, cpl_propertylist * proplist);

int hawki_bkg_from_objects_median
(const cpl_frameset  *  objframes, cpl_imagelist * bkg);

int hawki_bkg_from_sky_median
(const cpl_frameset     *  skyframes, 
 cpl_imagelist    *  bkg);

int hawki_bkg_from_running_mean
(cpl_imagelist       * objimages,
 const cpl_vector    * medians,
 int                   i_target,
 int                   half_width,
 int                   rejlow,
 int                   rejhigh,
 cpl_image           * bkg);

int hawki_bkg_from_running_mean_frame_extension
(hawki_bkg_frames_buffer * frames_buffer,
 cpl_vector              * offsets_x,
 cpl_vector              * offsets_y,
 cpl_image               * globalmask,
 double                    mask_off_x,
 double                    mask_off_y,
 cpl_image               * distortion_x,
 cpl_image               * distortion_y,
 int                       iextension,
 int                       i_target,
 int                       half_width,
 int                       rejlow,
 int                       rejhigh,
 cpl_image               * bkg);

int hawki_bkg_set_obj_mask
(cpl_image  * target_image,
 cpl_image  * globalmask,
 cpl_image  * distor_x,
 cpl_image  * distor_y,
 double       target_off_x,
 double       target_off_y,
 double       mask_off_x,
 double       mask_off_y);

CPL_END_DECLS

#endif
