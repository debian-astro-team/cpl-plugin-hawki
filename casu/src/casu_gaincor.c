/* $Id: casu_gaincor.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "catalogue/casu_fits.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Gain correct input data frame

    \par Name:
        casu_gaincor
    \par Purpose:
        Correct input data for inter-detector gain difference
    \par Description:
        An input image is scaled by a constant value so as to put all the 
        images in a single observation onto the same gain scale.
    \par Language:
        C
    \param infile 
        The input data image (overwritten by result).
    \param gainscl 
        The input scale factor
    \param status 
        An input/output status that is the same as the returned values below. 
    \retval CASU_OK 
        if everything is ok
    \retval CASU_WARN 
        if the scale factor is <= zero
    \retval CASU_FATAL 
        if image data fails to load
    \par QC headers:
        None
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b GAINCOR
            The gain correction factor
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_gaincor(casu_fits *infile, float gainscl, int *status) {
    cpl_image *i;
    cpl_propertylist *oplist;

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

    /* Do we need to be here? */

    if (cpl_propertylist_has(casu_fits_get_ehu(infile),"ESO DRS GAINCOR"))
        return(*status);

    /* Get the images and check the dimensions of each */

    i = casu_fits_get_image(infile);

    /* If the factor is zero or negative, then send a warning out and
       don't do the scaling. Otherwise scale the image */

    if (gainscl <= 0.0) {
        WARN_CONTINUE
    } else {
        cpl_image_multiply_scalar(i,gainscl);
    }

    /* Now put some stuff in the DRS extension... */

    oplist = casu_fits_get_ehu(infile);
    if (oplist != NULL) {
        cpl_propertylist_update_float(oplist,"ESO DRS GAINCOR",gainscl);
        cpl_propertylist_set_comment(oplist,"ESO DRS GAINCOR",
                                     "Gain correction factor");
    } else 
        WARN_CONTINUE

    /* Get out of here */

    return(*status);
}

/**@}*/

/*

$Log: casu_gaincor.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.4  2015/01/29 11:48:15  jim
modified comments

Revision 1.3  2014/03/26 15:42:10  jim
Fixed minor bug

Revision 1.2  2013/11/21 09:38:13  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
