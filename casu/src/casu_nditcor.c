/* $Id: casu_nditcor.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "catalogue/casu_fits.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Correct input data for number of dits
  
    \par Name:
        casu_nditcor
    \par Purpose:
        Correct input data for multiple dits.
    \par Description:
        Divide the input data by the number of dits. This is so that
        all images are scaled the same way. The result overwrites
        the input data array. The header exposure time is also modified.
    \par Language:
        C
    \param infile 
        The input data image (overwritten by result).
    \param ndit
        The number of dits in the observation
    \param expkey
        The FITS keyword for the exposure time
    \param status 
        An input/output status that is the same as the returned values below.
    \retval CASU_OK 
        if everything is ok
    \retval CASU_FATAL 
        if image data fails to load
    \par QC headers:
        None
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b NDITCOR
            A logical keyword to say that this correction has been applied
    \par Main headers:
        The following keywords in the non-hierarchical part of the headers
        have been modified:
        - \b expkey
            The exposure time on entry is divided by the number of DITs. This
            is done both in the extension and the primary header.
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_nditcor(casu_fits *infile, int ndit, const char *expkey,
                        int *status) {
    cpl_image *im;
    double dnd,exptime;
    cpl_propertylist *oplist;
    char comment[32];

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

    /* Do we need to be here? */

    if (cpl_propertylist_has(casu_fits_get_ehu(infile),"ESO DRS NDITCOR"))
        return(*status);

    /* Get the image and the scale factor */
    
    im = casu_fits_get_image(infile);
    dnd = (float)ndit;
    
    /* Divide it through... */

    cpl_image_divide_scalar(im,dnd);

    /* Now put some stuff in the DRS extension and update the exposure
       time to reflect this correction. The latter has to be done in
       both the extension and primary headers */

    oplist = casu_fits_get_ehu(infile);
    if (oplist != NULL) {
        cpl_propertylist_update_bool(oplist,"ESO DRS NDITCOR",1);
        (void)sprintf(comment,"Corrected for ndit=%d",ndit);
        cpl_propertylist_set_comment(oplist,"ESO DRS NDITCOR",comment);
        if (cpl_propertylist_has(oplist,expkey)) {
            exptime = cpl_propertylist_get_double(oplist,expkey);
            exptime /= dnd;
            cpl_propertylist_update_double(oplist,expkey,exptime);
            cpl_propertylist_set_comment(oplist,expkey,comment);
        }
    } 
    oplist = casu_fits_get_phu(infile);
    if (oplist != NULL && cpl_propertylist_has(oplist,expkey)) {
        (void)sprintf(comment,"Corrected for ndit=%d",ndit);
        exptime = cpl_propertylist_get_double(oplist,expkey);
        exptime /= dnd;
        cpl_propertylist_update_double(oplist,expkey,exptime);
        cpl_propertylist_set_comment(oplist,expkey,comment);
    } 

    /* Get out of here */

    GOOD_STATUS
}

/**@}*/

/*

$Log: casu_nditcor.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.4  2015/06/09 18:35:34  jim
Fixed exptime property

Revision 1.3  2015/01/29 11:51:56  jim
modified comments

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported

*/
