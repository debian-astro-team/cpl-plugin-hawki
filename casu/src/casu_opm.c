/* $Id: casu_opm.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "catalogue/casu_fits.h"
#include "casu_mask.h"
#include "catalogue/imcore.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Generate an object mask from an input image

    \par Name:
        casu_opm
    \par Purpose:
        Generate an object mask from an input image
    \par Description:
        A frame and its confidence map are given. Detection thresholds and
        various other parameters are also given. Output is an object mask of
        the extracted objects. The result is written to the input cpl_image
        bad pixel mask.
    \par Language:
        C
    \param infile
        The input frame with the image to be analysed
    \param conf
        The input frame with the confidence map
    \param ipix
        The minimum allowable size of an object
    \param threshold
        The detection threshold in sigma above sky
    \param nbsize
        The smoothing box size for background map estimation
    \param filtfwhm
        The FWHM of the smoothing kernel in the detection algorithm
    \param niter
        The number of detection iterations to be done
    \param status
        The input/output status that has the same return value a below
    \retval CASU_OK 
        if everything is ok
    \retval CASU_WARN,CASU_FATAL
        errors in the called routines
    \par DRS headers:
        The following values will go into the extension propertylist
        - \b SKYLEVEL
            Mean sky brightness in ADU
        - \b SKYNOISE
            Pixel noise at sky level in ADU
    \author
        Jim Lewis, CASU

 */
/*---------------------------------------------------------------------------*/

extern int casu_opm(casu_fits *infile, casu_fits *conf, int ipix,
                    float threshold, int nbsize, float filtfwhm, 
                    int niter, int *status) {
    int retval;
    cpl_image *im;
    casu_fits *in_copy,*c_copy;

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

    /* Copy the input */

    im = casu_fits_get_image(infile);
    in_copy = casu_fits_duplicate(infile);
    c_copy = casu_fits_duplicate(conf);

    /* Call the main processing routine and create the mask */

    retval = imcore_opm(in_copy,c_copy,ipix,threshold,nbsize,filtfwhm,niter);
    if (retval != CASU_OK) {
        casu_fits_delete(in_copy);
        casu_fits_delete(c_copy);
        FATAL_ERROR
    }   

    /* Copy the mask over */

    cpl_mask_or(cpl_image_get_bpm(im),
                cpl_image_get_bpm(casu_fits_get_image(in_copy)));

    /* Ditch the copies */

    casu_fits_delete(in_copy);
    casu_fits_delete(c_copy);

    /* Get out of here */

    GOOD_STATUS
}

/**@}*/

/* 

$Log: casu_opm.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.3  2015/01/29 11:51:56  jim
modified comments

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
