/* $Id: casu_mkconf.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "casu_mask.h"
#include "casu_stats.h"
#include "catalogue/casu_fits.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Create a confidence map

    \par Name:
        vircam_mkconf
    \par Purpose:
        Create a confidence map
    \par Description:
        Create a confidence map from a given flat image and a bad pixel mask.
        The confidence is defined as 100 times the normalised flat field. Any
        pixels that are flagged as bad will have a value of 0 confidence.
    \par Language:
        C
    \param flat
        The input flat field data image 
    \param flatfile
        The input flat field file name.
    \param bpm
        The input mask image
    \param outconf
        The ouput confidence map image
    \param drs
        A propertylist with entries for the drs header block
    \param status
        An input/output status that is the same as the returned values below.
    \retval CASU_OK
        if everything is ok
    \retval CASU_FATAL
        if image data fails to load or the dimensionality of the input images
        don't match.
    \par QC headers:
        None
    \par DRS headers:
        The following DRS keywords are written to the drs propertylist
        - \b FLATIN
            The name of the originating FITS file for the flat image
        - \b BPMIN: 
            The name of the originating FITS file for the BPM image
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_mkconf(cpl_image *flat, char *flatfile, casu_mask *bpm, 
                       cpl_image **outconf, cpl_propertylist **drs, 
                       int *status) {
    int i,nx,ny;
    long npts;
    unsigned char *bdata;
    float *fdata,mean;
    int *odata;
    const char *fctid = "casu_mkconf";

    /* Inherited status */

    *outconf = NULL;
    *drs = NULL;
    if (*status != CASU_OK)
        return(*status);

    /* Check the sizes of the input images to make sure they match */

    nx = (int)cpl_image_get_size_x(flat);
    ny = (int)cpl_image_get_size_y(flat);
    npts = nx*ny;
    if (casu_mask_get_size_x(bpm)*casu_mask_get_size_y(bpm) != npts) {
        cpl_msg_error(fctid,"Input image sizes don't match!");
        FATAL_ERROR
    }

    /* Get input the data arrays */

    if ((fdata = cpl_image_get_data_float(flat)) == NULL) {
        cpl_msg_error(fctid,"Unable to map flat data!");
        FATAL_ERROR
    }
    bdata = casu_mask_get_data(bpm);

    /* Get a data array for the output */

    odata = cpl_malloc(npts*sizeof(*odata));

    /* Work out the mean of the flat field. It should already be 1, but you
       never know... */

    mean = casu_mean(fdata,bdata,npts);

    /* Now create the output array */

    for (i = 0; i < npts; i++) {
        if (bdata[i] != 1) {
            odata[i] = max(0,min(110,(int)(100*fdata[i]/mean)));
            if (odata[i] < 20)
                odata[i] = 0;
        } else {
            odata[i] = 0;
        }
    }

    /* Wrap the data into an output image */

    *outconf = cpl_image_wrap_int((cpl_size)nx,(cpl_size)ny,odata);

    /* Create some properties */

    *drs = cpl_propertylist_new();
    cpl_propertylist_update_string(*drs,"ESO DRS FLATIN",flatfile);
    cpl_propertylist_set_comment(*drs,"ESO DRS FLATIN",
                                 "Flat used to create this conf map");
    if (casu_mask_get_type(bpm) != MASK_NONE && 
        casu_mask_get_filename(bpm) != NULL)
        cpl_propertylist_update_string(*drs,"ESO DRS BPMIN",
                                       casu_mask_get_filename(bpm));
    else
        cpl_propertylist_update_string(*drs,"ESO DRS BPMIN","None available");
    cpl_propertylist_set_comment(*drs,"ESO DRS BPMIN",
                                 "BPM used to create this conf map");

    /* Tidy up */

    GOOD_STATUS
}

/**@}*/

/*

$Log: casu_mkconf.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.6  2015/01/29 11:51:56  jim
modified comments

Revision 1.5  2015/01/09 12:13:15  jim
*** empty log message ***

Revision 1.4  2014/12/11 12:23:33  jim
new version

Revision 1.3  2014/03/26 15:51:53  jim
Creates floating point confidence maps now

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
