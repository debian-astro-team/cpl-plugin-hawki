/* $Id: casu_flatcor.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "casu_mods.h"
#include "catalogue/casu_utils.h"
#include "catalogue/casu_fits.h"

/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup casu_modules
    \brief Correct input data for flat field response

    \par Name:
        casu_flatcor
    \par Purpose:
        Correct input data for flat field response
    \par Description:
        Two images are given -- one is the data for an observation, the
        other is for a mean flat frame. The latter is divided into
        the former. The result overwrites the input data array for the object
        image. 
    \par Language:
        C
    \param infile 
        The input data image (overwritten by result).
    \param flatsrc 
        The input flat image
    \param status 
        An input/output status that is the same as the returned values below. 
    \retval CASU_OK 
        if everything is ok
    \retval CASU_WARN 
        if there is a zero divide or if the output property list is NULL.
    \retval CASU_FATAL 
        if image data fails to load or the division fails
    \par QC headers:
        None
    \par DRS headers:
        The following DRS keywords are written to the infile extension header
        - \b FLATCOR
            The name of the originating FITS file for the flat image
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int casu_flatcor(casu_fits *infile, casu_fits *flatsrc, int *status) {
    cpl_error_code cpl_retval;
    cpl_image *i,*f;
    cpl_propertylist *oplist;
    const char *fctid = "casu_flatcor";

    /* Inherited status */

    if (*status != CASU_OK)
        return(*status);

    /* Do we even need to be here */

    oplist = casu_fits_get_ehu(infile);
    if (cpl_propertylist_has(oplist,"ESO DRS FLATCOR"))
        return(*status);

    /* Get the images and check the dimensions of each */

    i = casu_fits_get_image(infile);
    f = casu_fits_get_image(flatsrc);
    if (casu_compare_dims(i,f) != CASU_OK) {
        cpl_msg_error(fctid,"Object and flat data array dimensions don't match");
        FATAL_ERROR
    }

    /* Use the cpl image routine to do the arithmetic */
        
    cpl_retval = cpl_image_divide(i,f);
    switch (cpl_retval) {
    case CPL_ERROR_NONE:
        break;
    case CPL_ERROR_DIVISION_BY_ZERO:
        cpl_error_reset();
        WARN_CONTINUE
        break;
    default:
        FATAL_ERROR
    }

    /* Now put some stuff in the DRS extension... */

    oplist = casu_fits_get_ehu(infile);
    if (oplist != NULL) {
        if (casu_fits_get_fullname(flatsrc) != NULL)  {
            cpl_propertylist_update_string(oplist,"ESO DRS FLATCOR",
                                           casu_fits_get_fullname(flatsrc));
            cpl_propertylist_set_comment(oplist,"ESO DRS FLATCOR",
                                         "Image used in flat correction");
        } else {
            cpl_propertylist_update_string(oplist,"ESO DRS FLATCOR",
                                           "Memory File");
        }
    } else 
        WARN_CONTINUE

    /* Get out of here */

    return(*status);
}


/**@}*/

/*

$Log: casu_flatcor.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.5  2015/01/29 11:48:15  jim
modified comments

Revision 1.4  2014/04/09 09:09:51  jim
Detabbed

Revision 1.3  2014/03/26 15:39:19  jim
Fixed so that input flat could be a memory file

Revision 1.2  2013/11/21 09:38:13  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
