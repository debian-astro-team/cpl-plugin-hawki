/* $Id: casu_stats.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

/* Includes */

#ifndef CASU_STATS_H
#define CASU_STATS_H

#include <cpl.h>

extern float casu_med(float *data, unsigned char *bpm, long npts);
extern double casu_dmed(double *data, unsigned char *bpm, long npts);
extern float casu_mean(float *data, unsigned char *bpm, long npts);
extern double casu_dmean(double *data, unsigned char *bpm, long npts);
extern void casu_qmedsig(float *data, unsigned char *bpm, long npts,
                         float thresh, int niter, float lowv, float highv,
                         float *median, float *sigma);
extern void casu_medmad(float *data, unsigned char *bpm, long np, float *med,
                        float *mad);
extern void casu_medmadcut(float *data, unsigned char *bpm, long np, float lcut,
                           float hcut, float *med, float *mad);
extern int casu_meansig(float *data, unsigned char *bpm, long npts, 
                        float *mean, float *sig);
extern void casu_medsig(float *data, unsigned char *bpm, long np, float *med,
                        float *sig);
extern int casu_sumbpm(unsigned char *bpm, long npts, int *sumb);

#endif

/*

$Log: casu_stats.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.4  2015/01/29 11:54:46  jim
Added medmadcut routine

Revision 1.3  2014/04/26 18:38:19  jim
changed argument declaration for sumbpm

Revision 1.2  2013/11/21 09:38:14  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
