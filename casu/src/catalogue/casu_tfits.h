/* $Id: casu_tfits.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */


/* Includes */

#ifndef CASU_TFITS_H
#define CASU_TFITS_H

#include <cpl.h>

/* CASU TFITS structure. */

typedef struct {
    cpl_table        *table;
    cpl_propertylist *phu;
    cpl_propertylist *ehu;
    char             *fname;
    char             *extname;
    char             *fullname;
    int              nexten;
    int              status;
} casu_tfits;

/* CASU_TFITS methods prototypes */

extern casu_tfits *casu_tfits_load(cpl_frame *frame, int nexten);
extern casu_tfits *casu_tfits_extract(casu_tfits *in);
extern casu_tfits *casu_tfits_duplicate(casu_tfits *in);
extern casu_tfits **casu_tfits_load_list(cpl_frameset *f, int exten);
extern void casu_tfits_delete(casu_tfits *p);
extern void casu_tfits_delete_list(casu_tfits **p, int n);
extern cpl_table *casu_tfits_get_table(casu_tfits *p);
extern int casu_tfits_get_nexten(casu_tfits *p);
extern char *casu_tfits_get_filename(casu_tfits *p);
extern char *casu_tfits_get_fullname(casu_tfits *p);
extern cpl_propertylist *casu_tfits_get_phu(casu_tfits *p);
extern cpl_propertylist *casu_tfits_get_ehu(casu_tfits *p);
extern int casu_tfits_get_status(casu_tfits *p);
extern void casu_tfits_set_status(casu_tfits *p, int status);
extern int casu_tfits_set_error(casu_tfits *p, int status);
extern void casu_tfits_set_filename(casu_tfits *p, char *fname);
extern casu_tfits *casu_tfits_wrap(cpl_table *im, casu_tfits *model,
                                   cpl_propertylist *phu, 
                                   cpl_propertylist *ehu);

#endif

/*

$Log: casu_tfits.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.4  2015/01/29 11:56:27  jim
modified comments

Revision 1.3  2013/11/21 09:38:14  jim
detabbed

Revision 1.2  2013-10-24 09:27:01  jim
added casu_tfits_set_status

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
