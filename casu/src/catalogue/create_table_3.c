/* $Id: create_table_3.c,v 1.3 2015/08/12 11:16:55 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/12 11:16:55 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <math.h>
#include "imcore.h"
#include "util.h"
#include "floatmath.h"

/* Column numbers for each item to be stored */

#define COL_NUMBER      1
#define COL_X           2
#define COL_Y           3
#define COL_FLUXISO     4
#define COL_PEAKHEIGHT  5
#define COL_ELLIPT      6
#define COL_SIGMA       7
#define COL_PA          8
#define COL_AREAL1      9
#define COL_AREAL2     10
#define COL_AREAL3     11
#define COL_AREAL4     12
#define COL_AREAL5     13
#define COL_AREAL6     14
#define COL_AREAL7     15
#define COL_AREAL8     16

/* Number of columns in the table */

#define NCOLS 32

/* Column definitions */

static const char *ttype[NCOLS]={"No.","X_coordinate","Y_coordinate",
                                 "Isophotal_flux","Peak_height","Ellipticity",
                                 "Gaussian_sigma","Position_angle",
                                 "Areal_1_profile","Areal_2_profile","Areal_3_profile",
                                 "Areal_4_profile","Areal_5_profile","Areal_6_profile",
                                 "Areal_7_profile","Areal_8_profile",
                                 "Blank17","Blank18","Blank19","Blank20",
                                 "Blank21","Blank22","Blank23","Blank24",
                                 "Blank25","Blank26","Blank27","Blank28",
                                 "Blank29","Blank30","Blank31","Blank32"};
static const char *tunit[NCOLS]={" ","Pixels","Pixels","Counts","Counts"," ",
                                 "Pixels","Degrees","Pixels","Pixels","Pixels",
                                 "Pixels","Pixels","Pixels","Pixels","Pixels",
                                 "Blank","Blank","Blank","Blank","Blank","Blank",
                                 "Blank","Blank","Blank","Blank","Blank","Blank",
                                 "Blank","Blank","Blank","Blank"};

static cpl_type tform[NCOLS]={CPL_TYPE_INT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,CPL_TYPE_FLOAT,
                              CPL_TYPE_FLOAT,CPL_TYPE_FLOAT};

static int areal_cols[NAREAL] = {COL_AREAL1,COL_AREAL2,COL_AREAL3,COL_AREAL4,
                                 COL_AREAL5,COL_AREAL6,COL_AREAL7,COL_AREAL8};
/**@{*/

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Initialise type 1 catalogue
  
    \par Name:
        imcore_tabinit_3
    \par Purpose:
        Initialise type 3 catalogue
    \par Description:
        Type 3 catalogue is initialised and the xy columns are identified.
    \par Language:
        C
    \returns
        Nothing
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern void imcore_tabinit_3(int *imcore_xcol, int *imcore_ycol, 
                             cpl_table **tab) {

    /* Call the generic routine to open a new output table */

    imcore_tabinit_gen(NCOLS,ttype,tunit,tform,tab);

    /* RA and DEC columns are undefined */

    *imcore_xcol = COL_X;
    *imcore_ycol = COL_Y;
}

/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Do seeing estimate for type 3 catalogue
  
    \par Name:
        imcore_do_seeing_3
    \par Purpose:
        Do seeing estimate for type 3 catalogue
    \par Description:
        Areal profiles in a type 3 catalogue are analysed and a seeing
        estimate is extracted
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all is well. Currently this is the only return value
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_do_seeing_3(ap_t *ap, int nobjects, cpl_table *tab) {
    int retval,i;
    char *areal_colnames[NAREAL];

    /* Sort out the areal profile column names */

    for (i = 0; i < NAREAL; i++) 
        areal_colnames[i] = (char *)ttype[areal_cols[i]-1];
 
    /* Just call the generic seeing routine */
 
    retval = imcore_do_seeing_gen(ap,ttype[COL_ELLIPT-1],ttype[COL_PEAKHEIGHT-1],
                                  areal_colnames,nobjects,tab);

    /* Get out of here */
 
    return(retval);
}
        
        
/*---------------------------------------------------------------------------*/
/**
    \ingroup cataloguemodules
    \brief Process results for type 3 catalogue
  
    \par Name:
        imcore_process_results_3
    \par Purpose:
        Create the results for objects in a type 3 catalogue
    \par Description:
        The pixel processing is done for all the parameters wanted for
        a type 3 catalogue
    \par Language:
        C
    \param ap
        The current ap structure
    \retval CASU_OK
        If all is well. 
    \retval CASU_FATAL
        If peak flux < 0
    \par QC headers:
        None
    \par DRS headers:
        None
    \author
        Jim Lewis, CASU
 */
/*---------------------------------------------------------------------------*/

extern int imcore_process_results_3(ap_t *ap, int *nobjects, cpl_table *tab) {
    float momresults[8],parmall[NPAR];
    float sxx,syy,srr,sxy,ecc,temp,xx,theta,radeg,ell;
    float yy,sigma,peak,areal1,iso_flux;
    float areal2,areal3,areal4,areal5,areal6,areal7,areal8;
    int iareal[NAREAL],i,nr;
    long nrows;

    /* Do a basic moments analysis and work out the areal profiles*/

    imcore_moments(ap,momresults);
    if (momresults[0] < 0)
        return(CASU_FATAL);
    imcore_areals(ap,iareal);

    /* See if this object makes the cut in terms of its size.  If not, then
       just return with good status */

    if (iareal[0] < ap->ipnop || momresults[3] < ap->xintmin)
        return(CASU_OK);

    /* Try and deblend the images if it is requested and justified */

    parmall[0] = momresults[3];
    parmall[1] = momresults[1];
    parmall[2] = momresults[2];
    parmall[3] = ap->thresh;
    for (i = 4; i < 8; i++) 
        parmall[i] = momresults[i];
    for (i = 0; i < NAREAL; i++)
        parmall[i+8] = (float)iareal[i];

    /* Massage the results and write them to the fits table */

    radeg = 180.0/CPL_MATH_PI;
    sxx = parmall[4];
    sxy = parmall[5];
    syy = parmall[6];    
    if(sxy > 0.0)
      sxy = MAX(1.0e-4,MIN(sxy,sqrtf(sxx*syy)));
    else
      sxy = MIN(-1.0e-4,MAX(sxy,-sqrtf(sxx*syy)));

    srr = MAX(0.5,sxx+syy);
    ecc = sqrtf((syy-sxx)*(syy-sxx)+4.0*sxy*sxy)/srr;
    temp = MAX((1.0-ecc)/(1.0+ecc),0.0);
    ell = 1.0 - sqrtf(temp);
    ell = MIN(0.99,MAX(0.0,ell));
    xx = 0.5*(1.0+ecc)*srr-sxx;
    if(xx == 0.0)
        theta = 0.0;
    else
        theta = 90.0-radeg*atanf(sxy/xx);

    /* Create a list of values */

    nrows = cpl_table_get_nrow(tab);
    (*nobjects)++;
    if (*nobjects > nrows) 
        (void)cpl_table_set_size(tab,nrows+INITROWS);
    nr = *nobjects - 1;
    iso_flux = parmall[0];
    xx = parmall[1];
    yy = parmall[2];
    sigma = sqrt(srr);
    peak = parmall[7];
    areal1 = parmall[8];
    areal2 = parmall[9];
    areal3 = parmall[10];
    areal4 = parmall[11];
    areal5 = parmall[12];
    areal6 = parmall[13];
    areal7 = parmall[14];
    areal8 = parmall[15];

    /* Store away the results for this object */

    cpl_table_set_int(tab,ttype[COL_NUMBER-1],nr,nr);
    cpl_table_set_float(tab,ttype[COL_X-1],nr,xx);
    cpl_table_set_float(tab,ttype[COL_Y-1],nr,yy);
    cpl_table_set_float(tab,ttype[COL_FLUXISO-1],nr,iso_flux);
    cpl_table_set_float(tab,ttype[COL_PEAKHEIGHT-1],nr,peak);
    cpl_table_set_float(tab,ttype[COL_ELLIPT-1],nr,ell);
    cpl_table_set_float(tab,ttype[COL_SIGMA-1],nr,sigma);
    cpl_table_set_float(tab,ttype[COL_PA-1],nr,theta);
    cpl_table_set_float(tab,ttype[COL_AREAL1-1],nr,areal1);
    cpl_table_set_float(tab,ttype[COL_AREAL2-1],nr,areal2);
    cpl_table_set_float(tab,ttype[COL_AREAL3-1],nr,areal3);
    cpl_table_set_float(tab,ttype[COL_AREAL4-1],nr,areal4);
    cpl_table_set_float(tab,ttype[COL_AREAL5-1],nr,areal5);
    cpl_table_set_float(tab,ttype[COL_AREAL6-1],nr,areal6);
    cpl_table_set_float(tab,ttype[COL_AREAL7-1],nr,areal7);
    cpl_table_set_float(tab,ttype[COL_AREAL8-1],nr,areal8);

    /* Get outta here */

    return(CASU_OK);
}

/**@}*/

/*

$Log: create_table_3.c,v $
Revision 1.3  2015/08/12 11:16:55  jim
Modified procedure names to protect namespace

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.3  2015/01/09 11:42:36  jim
Fixed routines to remove globals

Revision 1.2  2014/04/09 09:09:51  jim
Detabbed

Revision 1.1.1.1  2013/08/27 12:07:48  jim
Imported


*/
