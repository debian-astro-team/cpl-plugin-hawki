/* $Id: casu_filt.h,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#ifndef CASU_FILT_H
#define CASU_FILT_H

/* Macros for defining stats to be done */

#define MEDIANCALC 1
#define MEANCALC 2

/* Function prototypes */

extern void casu_bfilt(float *data, unsigned char *bpm, int nx, int ny,
                       int filt, int stat, int axis);
extern void casu_dostat(float *data, unsigned char *bpm, 
                        unsigned char *goodval, int npts, int nfilt, 
                        int whichstat);
#endif

/* 

$Log: casu_filt.h,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.5  2015/06/09 18:31:56  jim
Made casu_dostat external

Revision 1.4  2015/01/29 11:48:15  jim
modified comments

Revision 1.3  2014/03/26 15:34:44  jim
Removed extraneous declaration

Revision 1.2  2013/11/21 09:38:13  jim
detabbed

Revision 1.1.1.1  2013-08-27 12:07:48  jim
Imported


*/
