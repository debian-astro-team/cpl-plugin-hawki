/* $Id: casu_opm-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval;
    cpl_image *im;
    casu_fits *ff,*ffc;
    cpl_mask *bpm_mask;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    retval = casu_opm(NULL,NULL,5,3,64,1.0,1,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);

    /* Create an image and a confidence map */

    im = cpl_image_new(1024,1024,CPL_TYPE_FLOAT);
    cpl_image_fill_noise_uniform(im,990.0,1010.0);
    cpl_image_fill_window(im,500,500,505,505,5000);
    ff = casu_fits_wrap(im,NULL,NULL,NULL);
    im = cpl_image_new(1024,1024,CPL_TYPE_INT);
    cpl_image_fill_noise_uniform(im,95.0,105.0);
    ffc = casu_fits_wrap(im,NULL,NULL,NULL);

    /* Do an analysis now */

    status = CASU_OK;
    retval = casu_opm(ff,ffc,5,10.0,64,1.0,1,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    bpm_mask = cpl_image_get_bpm(casu_fits_get_image(ff));
    cpl_test_eq(60,(int)cpl_mask_count(bpm_mask));

    /* Tidy and exit */

    casu_fits_delete(ff);
    casu_fits_delete(ffc);
    return(cpl_test_end(0));
}

/*

$Log: casu_opm-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
