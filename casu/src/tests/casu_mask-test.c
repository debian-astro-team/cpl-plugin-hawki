/* $Id: casu_mask-test.c,v 1.3 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>
#include <casu_mask.h>

int main(void) {
    int retval,ok,i;
    cpl_image *im;
    cpl_frameset *frms;
    cpl_frame *fr;
    cpl_propertylist *phu,*ehu;
    cpl_size *labels,nlab;
    casu_mask *cm;
    unsigned char *bpm;
    const char *fname = "testmask.fit";

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Create a dummy confidence map with two bad pixels at (1,1) and
       at (2,1) */

    im = cpl_image_new(100,100,CPL_TYPE_INT);
    cpl_image_fill_noise_uniform(im,95.0,105.0);
    cpl_image_set(im,1,1,0.0);
    cpl_image_set(im,2,1,0.0);
    phu = cpl_propertylist_new();
    ehu = cpl_propertylist_new();
    if (access(fname,F_OK) == 0)
        remove(fname);
    cpl_image_save(NULL,fname,CPL_TYPE_UCHAR,phu,CPL_IO_DEFAULT);
    cpl_image_save(im,fname,CPL_TYPE_INT,ehu,CPL_IO_EXTEND);
    cpl_propertylist_delete(phu);
    cpl_propertylist_delete(ehu);
    cpl_image_delete(im);

    /* Create a frameset with this file in it. Labelise it */

    frms = cpl_frameset_new();
    fr = cpl_frame_new();
    cpl_frame_set_filename(fr,fname);
    cpl_frame_set_tag(fr,"testconf");
    cpl_frameset_insert(frms,fr);
    labels = cpl_frameset_labelise(frms,casu_compare_tags,&nlab);

    /* Define the mask now and test the accessor routines */

    cm = casu_mask_define(frms,labels,nlab,"testconf","testbpm");
    cpl_test_nonnull(cm);
    cpl_test_eq_string(fname,casu_mask_get_filename(cm));
    cpl_test_eq(casu_mask_get_type(cm),MASK_CPM);
    cpl_free(labels);

    /* Load the mask and test the rest of the accessors */

    retval = casu_mask_load(NULL,1,-1,-1);
    cpl_test_eq(retval,CASU_FATAL);
    retval = casu_mask_load(cm,1,-1,-1);
    cpl_test_eq(retval,CASU_OK);
    cpl_test_nonnull(casu_mask_get_fits(cm));
    cpl_test_eq(casu_mask_get_size_x(cm),100);
    cpl_test_eq(casu_mask_get_size_y(cm),100);

    /* Get the data and make sure the values are correct. They should all
       be zero except for the first two */

    bpm = casu_mask_get_data(cm);
    cpl_test_nonnull(bpm);
    ok = 1;
    for (i = 0; i < 10000; i++) {
        if (i < 2 && bpm[i] != 1) {
            ok = 0;
            break;
        } else if (i >= 2 && bpm[i] != 0) {
            ok = 0;
            break;
        }
    }
    cpl_test_eq(ok,1);

    /* Tidy and exit */

    casu_mask_delete(cm); 
    remove(fname);
    cpl_frameset_delete(frms);
    return(cpl_test_end(0));
}

/*

$Log: casu_mask-test.c,v $
Revision 1.3  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.2  2015/08/06 05:34:02  jim
Fixes to get rid of compiler moans

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
