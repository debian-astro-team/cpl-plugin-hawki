/* $Id: casu_inpaint-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval;
    cpl_image *im;
    cpl_mask *bpm;
    casu_fits *ff;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    retval = casu_inpaint(NULL,64,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);

    /* Create an input file */

    im = cpl_image_new(1024,1024,CPL_TYPE_FLOAT);
    cpl_image_fill_noise_uniform(im,990.0,1010.0);
    cpl_image_fill_window(im,500,500,505,505,7000.0);
    bpm = cpl_image_get_bpm(im);
    cpl_mask_threshold_image(bpm,im,6090,7010,CPL_BINARY_1);            
    ff = casu_fits_wrap(im,NULL,NULL,NULL);

    /* Now test it */

    status = CASU_OK;
    retval = casu_inpaint(ff,64,&status);
    cpl_mask_threshold_image(bpm,im,6090,7010,CPL_BINARY_1);            
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_rel(1000.0,cpl_image_get_mean_window(im,500,500,505,505),0.001);

    /* Tidy and exit */

    casu_fits_delete(ff);
    return(cpl_test_end(0));
}

/*

$Log: casu_inpaint-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
