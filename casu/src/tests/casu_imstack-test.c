/* $Id: casu_imstack-test.c,v 1.3 2015/11/27 12:39:55 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/11/27 12:39:55 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_tfits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval;
    casu_fits *out,*outc,*outv;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status only. This is too complicated to do a noddy
       test. Do a proper regression test later */

    status = CASU_FATAL;
    retval = casu_imstack(NULL,NULL,NULL,NULL,100,100,5.0,5.0,1,0,1,0,
                          "EXPTIME",&out,&outc,&outv,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(out);
    cpl_test_null(outc);
    cpl_test_null(outv);
    status = CASU_OK;
    retval = casu_imstack(NULL,NULL,NULL,NULL,0,100,5.0,5.0,1,0,1,0,
                          "EXPTIME",&out,&outc,&outv,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(out);
    cpl_test_null(outc);
    cpl_test_null(outv);

    return(cpl_test_end(0));
}

/*

$Log: casu_imstack-test.c,v $
Revision 1.3  2015/11/27 12:39:55  jim
Added extra unmap parameter to imstack calls

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
