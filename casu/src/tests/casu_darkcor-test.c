/* $Id: casu_darkcor-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval;
    casu_fits *in,*dark;
    cpl_image *inim,*darkim;
    cpl_propertylist *ehu;
    double mean,stdev;
    float scl;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    in = NULL;
    dark = NULL;
    scl = 1.0;
    retval = casu_darkcor(in,dark,scl,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);

    /* Create some images. Create them a different sizes */

    inim = cpl_image_new(10,10,CPL_TYPE_FLOAT);
    darkim = cpl_image_new(11,11,CPL_TYPE_FLOAT);
    ehu = cpl_propertylist_new();
    cpl_propertylist_update_string(ehu,"ESO DRS DARKCOR","testfile");
    in = casu_fits_wrap(inim,NULL,NULL,ehu);
    cpl_propertylist_delete(ehu);
    ehu = casu_fits_get_ehu(in);
    dark = casu_fits_wrap(darkim,NULL,NULL,NULL);
    status = CASU_OK;
    
    /* Status should be OK if it thinks it's already corrected the image */

    retval = casu_darkcor(in,dark,scl,&status);
    cpl_test_eq(status,CASU_OK);    
    cpl_test_eq(status,retval);    

    /* Check to make sure it fails because of difference in image dimensions */

    cpl_propertylist_erase(ehu,"ESO DRS DARKCOR");
    retval = casu_darkcor(in,dark,scl,&status);
    cpl_test_eq(status,CASU_FATAL);    
    cpl_test_eq(status,retval);    

    /* Set up the images to have some data in them and they are the same 
       dimensionality */

    cpl_image_add_scalar(inim,10.0);
    casu_fits_unwrap(dark);
    cpl_image_delete(darkim);
    darkim = cpl_image_new(10,10,CPL_TYPE_FLOAT);
    cpl_image_add_scalar(darkim,2.0);
    dark = casu_fits_wrap(darkim,NULL,NULL,NULL);
    status = CASU_OK;
    
    /* Test to see if you get the right answer */

    retval = casu_darkcor(in,dark,scl,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    mean = cpl_image_get_mean((const cpl_image *)inim);
    stdev = cpl_image_get_stdev((const cpl_image *)inim);
    cpl_test_rel(mean,8.0,1.0e-6);
    cpl_test_rel(stdev,0.0,1.0e-6);

    /* Redo it all now with a different scale factor and see if you
       get the right answer */

    scl = 2.0;
    cpl_image_add_scalar(inim,2.0);
    cpl_propertylist_erase(ehu,"ESO DRS DARKCOR");
    retval = casu_darkcor(in,dark,scl,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    mean = cpl_image_get_mean((const cpl_image *)inim);
    stdev = cpl_image_get_stdev((const cpl_image *)inim);
    cpl_test_rel(mean,6.0,1.0e-6);
    cpl_test_rel(stdev,0.0,1.0e-6);
    cpl_test_eq_string("Memory File",
		       cpl_propertylist_get_string(ehu,"ESO DRS DARKCOR"));

    /* Tidy up and get out of here */

    casu_fits_unwrap(in);
    casu_fits_unwrap(dark);
    cpl_image_delete(inim);
    cpl_image_delete(darkim);

    return(cpl_test_end(0));
}

/*

$Log: casu_darkcor-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2014/03/26 15:31:29  jim
New Entry


*/
