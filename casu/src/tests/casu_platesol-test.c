/* $Id: casu_platesol-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval,i;
    cpl_propertylist *plist;
    cpl_table *matched;
    const char *reqcols[] = {"X_coordinate","Y_coordinate","xpredict",
                             "ypredict","RA","Dec"};
    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Checks here are limited. This needs a regression test. Check 
       inherited status first */

    plist = cpl_propertylist_new();
    matched = cpl_table_new(2);
    cpl_table_new_column(matched,"yuk",CPL_TYPE_FLOAT);
    status = CASU_FATAL;
    retval = casu_platesol(plist,NULL,matched,6,0,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,status);

    /* Check that it fails if you don't have the right columns in the
       input table */

    status = CASU_OK;
    retval = casu_platesol(plist,NULL,matched,4,0,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,status);

    /* Now put the correct columns in and see if it fails if we give it
       the wrong number of coefficients */
    
    for (i = 0; i < 6; i++)
        cpl_table_new_column(matched,reqcols[i],CPL_TYPE_FLOAT);
    status = CASU_OK;
    retval = casu_platesol(plist,NULL,matched,3,0,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,status);

    /* Finally increase the coefficients to 6 and it should fail again
       because there aren't enough standards in the table */

    status = CASU_OK;
    retval = casu_platesol(plist,NULL,matched,6,0,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(retval,status);

    /* Tidy and exit */

    cpl_table_delete(matched);
    cpl_propertylist_delete(plist);
    return(cpl_test_end(0));
}

/*

$Log: casu_platesol-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
