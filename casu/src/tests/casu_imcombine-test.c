/* $Id: casu_imcombine-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval;
    cpl_image *im,*outim,*outimv;
    casu_fits *infits[10];
    unsigned char *rejmask,*rejplus;
    cpl_propertylist *drs,*phu,*ehu;
    int i;
    double val1,val2,ave;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    retval = casu_imcombine(NULL,NULL,100,2,1,0,100.0,"EXPTIME",&outim,
                            &outimv,&rejmask,&rejplus,&drs,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(outim);
    cpl_test_null(outimv);
    cpl_test_null(rejmask);
    cpl_test_null(rejplus);
    cpl_test_null(drs);
    status = CASU_OK;

    /* Create some images to stack */

    for (i = 0; i < 10; i++) {
        im = cpl_image_new(100,100,CPL_TYPE_FLOAT);
        if (i % 2 == 0) {
            ave = 100.0;
        } else {
            ave = 200.0;
        }
        val1 = ave - 10.0;
        val2 = ave + 10.0;
        cpl_image_fill_noise_uniform(im,val1,val2);
        phu = cpl_propertylist_new();
        ehu = cpl_propertylist_new();
        cpl_propertylist_update_float(phu,"EXPTIME",ave);
        infits[i] = casu_fits_wrap(im,NULL,phu,ehu);
        cpl_propertylist_delete(phu);
        cpl_propertylist_delete(ehu);
    }

    /* Test the routine with straight-forward averaging and additive 
       scaling */

    retval = casu_imcombine(infits,NULL,10,2,1,0,100.0,"EXPTIME",&outim,
                            &outimv,&rejmask,&rejplus,&drs,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(outim);
    cpl_test_null(outimv);
    cpl_test_nonnull(rejmask);
    cpl_test_nonnull(rejplus);
    cpl_test_nonnull(drs);
    cpl_test_rel(cpl_image_get_mean(outim),150.0,0.001);
    cpl_image_delete(outim);
    cpl_free(rejmask);
    cpl_free(rejplus);
    cpl_propertylist_delete(drs);

    /* Test the routine with straight-forward averaging and multiplicative
       scaling */

    retval = casu_imcombine(infits,NULL,10,2,2,0,100.0,"EXPTIME",&outim,
                            &outimv,&rejmask,&rejplus,&drs,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(outim);
    cpl_test_null(outimv);
    cpl_test_nonnull(rejmask);
    cpl_test_nonnull(rejplus);
    cpl_test_nonnull(drs);
    cpl_test_rel(cpl_image_get_mean(outim),150.0,0.001);
    cpl_image_delete(outim);
    cpl_free(rejmask);
    cpl_free(rejplus);
    cpl_propertylist_delete(drs);

    /* Test the routine with straight-forward averaging and exposure time
       scaling */

    retval = casu_imcombine(infits,NULL,10,2,3,0,100.0,"EXPTIME",&outim,
                            &outimv,&rejmask,&rejplus,&drs,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(outim);
    cpl_test_null(outimv);
    cpl_test_nonnull(rejmask);
    cpl_test_nonnull(rejplus);
    cpl_test_nonnull(drs);
    cpl_test_rel(cpl_image_get_mean(outim),100.0,0.001);
    cpl_image_delete(outim);
    cpl_free(rejmask);
    cpl_free(rejplus);
    cpl_propertylist_delete(drs);

    /* Tidy and exit */

    for (i = 0; i < 10; i++)
        casu_fits_delete(infits[i]);
    return(cpl_test_end(0));
}

/*

$Log: casu_imcombine-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
