/* $Id: casu_nditcor-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval;
    cpl_image *im;
    cpl_propertylist *phu,*ehu;
    casu_fits *ff;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    retval = casu_nditcor(NULL,2,"EXPTIME",&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);

    /* Make a file to be corrected */

    im = cpl_image_new(100,100,CPL_TYPE_FLOAT);
    cpl_image_fill_window(im,1,1,100,100,200.0);
    phu = cpl_propertylist_new();
    cpl_propertylist_update_double(phu,"EXPTIME",20.0);
    ehu = cpl_propertylist_new();
    cpl_propertylist_update_double(ehu,"EXPTIME",20.0);
    cpl_propertylist_update_bool(ehu,"ESO DRS NDITCOR",CPL_TRUE);
    ff = casu_fits_wrap(im,NULL,phu,ehu);
    cpl_propertylist_delete(phu);
    cpl_propertylist_delete(ehu);

    /* Ok this file has been made to look as though it's already been
       corrected. See if the routine rejects it for processing on that 
       basis */

    status = CASU_OK;
    retval = casu_nditcor(ff,2,"EXPTIME",&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_rel(200.0,cpl_image_get_mean(casu_fits_get_image(ff)),0.001);
    cpl_test_eq(20.0,cpl_propertylist_get_double(casu_fits_get_phu(ff),"EXPTIME"));
    cpl_test_eq(20.0,cpl_propertylist_get_double(casu_fits_get_ehu(ff),"EXPTIME"));

    /* Right, erase the flag saying it's already been done. Then rerun and
       see if we get the right answer */

    cpl_propertylist_erase(casu_fits_get_ehu(ff),"ESO DRS NDITCOR");
    status = CASU_OK;
    retval = casu_nditcor(ff,2,"EXPTIME",&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_rel(100.0,cpl_image_get_mean(casu_fits_get_image(ff)),0.001);
    cpl_test_eq(10.0,cpl_propertylist_get_double(casu_fits_get_phu(ff),"EXPTIME"));
    cpl_test_eq(10.0,cpl_propertylist_get_double(casu_fits_get_ehu(ff),"EXPTIME"));
    cpl_test_eq(1,cpl_propertylist_has(casu_fits_get_ehu(ff),"ESO DRS NDITCOR"));

    /* Tidy and exit */

    casu_fits_delete(ff);
    return(cpl_test_end(0));
}

/*

$Log: casu_nditcor-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
