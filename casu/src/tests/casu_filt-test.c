/* $Id: casu_filt-test.c,v 1.3 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdlib.h>
#include <string.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_utils.h>
#include <casu_filt.h>

#define DATSZ 21

static void initdata(float *data);
/* static void spewdata(float *data); */

int main(void) {
    float *data;
    unsigned char *bpm;
    int nbpm,testpt;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Get some data arrays */

    data = cpl_malloc(DATSZ*DATSZ*sizeof(float));
    bpm = cpl_calloc(DATSZ*DATSZ,sizeof(float));
    nbpm = DATSZ*DATSZ/2;
    testpt = nbpm + DATSZ;

    /* Test mean calc with no bad pixels */

    initdata(data);
    casu_bfilt(data,bpm,DATSZ,DATSZ,5,MEANCALC,1);
    /* spewdata(data); */
    cpl_test_rel(data[testpt],111.0,1.0e-6);

    /* Test median calc with no bad pixels */

    initdata(data);
    casu_bfilt(data,bpm,DATSZ,DATSZ,5,MEDIANCALC,1);
    /* spewdata(data); */
    cpl_test_rel(data[testpt],111.0,1.0e-6);

    /* Test mean calc with a bad pixel in the middle */

    initdata(data);
    nbpm = DATSZ*DATSZ/2;
    bpm[nbpm] = 1;
    casu_bfilt(data,bpm,DATSZ,DATSZ,5,MEANCALC,1);
    /* spewdata(data); */
    cpl_test_rel(data[testpt],111.25,1.0e-6);

    /* Test median calc with a bad pixel in the middle */

    initdata(data);
    memset(bpm,0,DATSZ*DATSZ);
    bpm[nbpm] = 1;
    casu_bfilt(data,bpm,DATSZ,DATSZ,5,MEDIANCALC,1);
    cpl_test_rel(data[testpt],111.5,1.0e-6);
    /* spewdata(data); */

    /* Tidy and exit */

    cpl_free(data);
    cpl_free(bpm);
    return(cpl_test_end(0));
}

static void initdata(float *data) {
    int i,j,n,mid;
    float val;
    
    mid = DATSZ/2;
    
    n = -1;
    for (j = 0; j <= DATSZ-1; j++) {
	for (i = 0; i <= DATSZ-1; i++) {
	    n++;
	    if (i >= mid-3 && i <= mid+3 && j >= mid-3 && j <= mid+3) {
		val = i*10.0 + j;
	    } else {
		val = (mid-4)*10.0;
	    }
	    data[n] = val;
        }
    }
}

/* static void spewdata(float *data) { */
/*     int i,j,n,mid; */
/*     float val; */
    
/*     mid = DATSZ/2; */
    
/*     n = -1; */
/*     for (j = 0; j <= DATSZ-1; j++) { */
/* 	for (i = 0; i <= DATSZ-1; i++) { */
/* 	    n++; */
/* 	    val = data[n]; */
/* 	    fprintf(stderr,"%5.2f ",val); */
/*         } */
/* 	fprintf(stderr,"\n"); */
/*     } */
/*     fprintf(stderr,"\n"); */
/* } */

/*

$Log: casu_filt-test.c,v $
Revision 1.3  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.2  2015/08/06 05:34:02  jim
Fixes to get rid of compiler moans

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.2  2015/03/12 09:16:51  jim
Modified to remove some compiler moans

Revision 1.1  2014/03/26 15:31:29  jim
New Entry


*/
