/* $Id: casu_mkconf-test.c,v 1.2 2015/08/07 13:06:54 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/08/07 13:06:54 $
 * $Revision: 1.2 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval,nzero,i,*data;
    cpl_image *outconf;
    cpl_propertylist *drs;
    cpl_image *im;
    unsigned char *bpm;
    casu_mask *cm;

    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Check inherited status */

    status = CASU_FATAL;
    retval = casu_mkconf(NULL,NULL,NULL,&outconf,&drs,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(outconf);
    cpl_test_null(drs);

    /* Make some input files. Start with the mask, then make a normalised
       flat */

    bpm = cpl_calloc(100*100,sizeof(unsigned char));
    bpm[0] = 1;
    bpm[1] = 1;
    cm = casu_mask_wrap_bpm(bpm,100,100);
    im = cpl_image_new(100,100,CPL_TYPE_FLOAT);
    cpl_image_fill_noise_uniform(im,0.9,1.1);
    status = CASU_OK;
    retval = casu_mkconf(im,"testflat.fit",cm,&outconf,&drs,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(outconf);
    cpl_test_nonnull(drs);
    cpl_test_rel(100.0,cpl_image_get_mean(outconf),0.05);
    nzero = 0;
    data = cpl_image_get_data_int(outconf);
    for (i = 0; i < 10000; i++)
        if (data[i] == 0)
            nzero++;
    cpl_test_eq(nzero,2);

    /* Tidy and exit */
    
    cpl_propertylist_delete(drs);
    cpl_image_delete(outconf);
    casu_mask_delete(cm);
    cpl_image_delete(im);

    return(cpl_test_end(0));
}

/*

$Log: casu_mkconf-test.c,v $
Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
