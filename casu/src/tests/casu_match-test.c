/* $Id: casu_match-test.c,v 1.3 2015/10/08 10:05:47 jim Exp $
 *
 * This file is part of the CASU Pipeline utilities
 * Copyright (C) 2015 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: jim $
 * $Date: 2015/10/08 10:05:47 $
 * $Revision: 1.3 $
 * $Name:  $
 */

#include <stdio.h>
#include <stdlib.h>

#include <cpl_init.h>
#include <cpl_test.h>
#include <casu_fits.h>
#include <casu_utils.h>
#include <casu_mods.h>

int main(void) {
    int status,retval,i,nm;
    cpl_table *progtab,*temptab,*outtab;
    float *xptr,*yptr,xoff,yoff;
    float offx[20] = {0.10,-0.07,0.03,0.21,-0.15,-0.02,-0.08,0.11,0.13,-0.16,
                      0.01,-0.18,0.20,-0.14,-0.10,0.08,0.12,-0.05,0.04,-0.06};
    float offy[20] = {0.01,-0.18,0.20,-0.14,-0.10,0.08,0.12,-0.05,0.04,-0.06,
                      0.10,-0.07,0.03,0.21,-0.15,-0.02,-0.08,0.11,0.13,-0.16};
    /* Initialise */

    cpl_test_init(PACKAGE_BUGREPORT,CPL_MSG_WARNING);

    /* Do casu_matchxy first. Start by creating some tables */

    progtab = cpl_table_new(20);
    cpl_table_new_column(progtab,"X_coordinate",CPL_TYPE_FLOAT);
    cpl_table_new_column(progtab,"Y_coordinate",CPL_TYPE_FLOAT);
    xptr = cpl_table_get_data_float(progtab,"X_coordinate");
    yptr = cpl_table_get_data_float(progtab,"Y_coordinate");
    for (i = 0; i < 20; i++) {
        xptr[i] = (float)(i*(i+1));
        yptr[i] = (float)(i*(i-1));
    }
    temptab = cpl_table_new(20);
    cpl_table_new_column(temptab,"X_coordinate",CPL_TYPE_FLOAT);
    cpl_table_new_column(temptab,"Y_coordinate",CPL_TYPE_FLOAT);
    xptr = cpl_table_get_data_float(temptab,"X_coordinate");
    yptr = cpl_table_get_data_float(temptab,"Y_coordinate");
    for (i = 0; i < 20; i++) {
        xptr[i] = (float)(i*(i+1)) + 50.0 + offx[i];
        yptr[i] = (float)(i*(i-1)) - 40.0 + offy[i];
    }

    /* Test inherited status */

    status = CASU_FATAL;
    retval = casu_matchxy(progtab,temptab,10,&xoff,&yoff,&nm,&outtab,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_eq(xoff,0.0);
    cpl_test_eq(yoff,0.0);
    cpl_test_eq(nm,0);
    cpl_test_null(outtab);

    /* Now test to see if you get the right match */

    status = CASU_OK;
    retval = casu_matchxy(progtab,temptab,20,&xoff,&yoff,&nm,&outtab,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_rel(xoff,50.0,0.01);
    cpl_test_rel(yoff,-40.0,0.01);
    cpl_test_eq(nm,20);
    cpl_test_nonnull(outtab);
    cpl_test_eq(cpl_table_get_ncol(outtab),4);
    cpl_table_delete(outtab);

    /* Now add some columns to the prog table to simulate it being an
       imcore table. */

    cpl_table_new_column(progtab,"RA",CPL_TYPE_FLOAT);
    cpl_table_new_column(progtab,"DEC",CPL_TYPE_FLOAT);
    cpl_table_new_column(progtab,"FLUX1",CPL_TYPE_FLOAT);
    cpl_table_new_column(progtab,"FLUX2",CPL_TYPE_FLOAT);
    cpl_table_name_column(temptab,"X_coordinate","xpredict");
    cpl_table_name_column(temptab,"Y_coordinate","ypredict");
    status = CASU_FATAL;
    retval = casu_matchstds(progtab,temptab,10,&outtab,&status);
    cpl_test_eq(status,CASU_FATAL);
    cpl_test_eq(status,retval);
    cpl_test_null(outtab);
    status = CASU_OK;
    retval = casu_matchstds(progtab,temptab,10,&outtab,&status);
    cpl_test_eq(status,CASU_OK);
    cpl_test_eq(status,retval);
    cpl_test_nonnull(outtab);
    cpl_test_eq(cpl_table_get_ncol(outtab),6);

    /* Tidy and exit */

    cpl_table_delete(progtab);
    cpl_table_delete(temptab);
    cpl_table_delete(outtab);
    return(cpl_test_end(0));
}

/*

$Log: casu_match-test.c,v $
Revision 1.3  2015/10/08 10:05:47  jim
Modified the way we do positional errors

Revision 1.2  2015/08/07 13:06:54  jim
Fixed copyright to ESO

Revision 1.1.1.1  2015/06/12 10:44:32  jim
Initial import

Revision 1.1  2015/01/09 11:39:55  jim
new entry


*/
